//
//  OMDebugView.cpp
//  comspot
//
//  Created by owi on 13.03.13.
//
//

#include "OMDebugView.h"
#include "ofApp.h"

// ------------------------------------------------
void OMDebugView::setup() {
    
    tastenfeld.loadImage("tastenfeld.png");
    trippleClickTimestamp = ofGetElapsedTimeMillis();
    trippleCount = 0;
    keyPadStartedTimestamp = ofGetElapsedTimeMillis();
    keypadMode = false;
    
    // some variables are in testApp
    ofApp* t = (ofApp*) ofGetAppPtr();
    
    
    
    // xml settings  - - - - - - - - - - - - - - - - - - - -
    
//    XML.loadFile("settings.xml");
    XML.loadFile(ofxiPhoneGetDocumentsDirectory()+"settings.xml");
    

    
    bookmark_deactivate_twofingers = XML.getValue("BOOKMARK_DEACTIVATE_TWOFINGERS", false);
    bookmark_activation_sound = XML.getValue("BOOKMARK_ACTIVATION_SOUND", false);
    bookmark_presense_sound = XML.getValue("BOOKMARK_PRESENSE_SOUND", false);
    hysteresis_detection = XML.getValue("HYSTERESIS_DETECTION", false);
    hysteresis_low = XML.getValue("HYSTERESIS_LOW", 0.2);
    hysteresis_high = XML.getValue("HYSTERESIS_HIGH", 0.8);
    better_gestures = XML.getValue("BETTER_GESTURES", false);
    
    // internal
    
    debug = false;
    debugViewStartedTimestamp = ofGetElapsedTimeMillis();

    
	

    // listeners - - - - - - - - - - - - - - -
    
    ofAddListener(ofEvents().update, this, &OMDebugView::update);
    ofAddListener(ofEvents().draw, this, &OMDebugView::draw);
    ofAddListener(ofEvents().exit, this, &OMDebugView::exit);
    
    ofAddListener(ofEvents().keyPressed, this, &OMDebugView::keyPressed);
    
    ofAddListener(ofEvents().mousePressed, this, &OMDebugView::pressed);
    ofAddListener(ofEvents().mouseDragged, this, &OMDebugView::dragged);
    ofAddListener(ofEvents().mouseReleased, this, &OMDebugView::released);
    
    
    // UI - - - - - - - - - - - - - - - - - - - - -
    
    float hSpace = OFX_UI_GLOBAL_WIDGET_SPACING;
    float vSpace = OFX_UI_GLOBAL_WIDGET_SPACING;
    float width = 850;
    float height = 1000;
    float dim = 48;
    float blockSpace = 12;

    
    // OPTIONS - - - - - - - - - - - - - - - - - - - - -
    
    options = new ofxUICanvas(0, 0, width+hSpace, height);
    options->setColorBack(ofColor(0, 220));
    options->setVisible(debug);
    options->addWidgetDown(new ofxUILabel("OPTIONS", OFX_UI_FONT_LARGE));
   
    //options->addSpacer(width-hSpace, 2);
    //options->addWidgetDown(new ofxUIFPS(OFX_UI_FONT_LARGE));
    //options->addWidgetDown(new ofxUIFPSSlider("FPS", width-hSpace, dim, 120));

    options->addSpacer(width-hSpace, 2);
    options->addWidgetDown(new ofxUIToggle("Bookmark deactivation only with 2 Fingers", bookmark_deactivate_twofingers, dim, dim));
    options->addWidgetDown(new ofxUIToggle("Bookmark activation sound", bookmark_activation_sound, dim, dim));
    options->addWidgetDown(new ofxUIToggle("Bookmark pre sense sound", bookmark_presense_sound, dim, dim));
    options->addWidgetDown(new ofxUIToggle("Tighter gesture separation", better_gestures, dim, dim));
    
    options->addSpacer(width-hSpace, 2);
    options->addWidgetDown(new ofxUIToggle("Touch hysteresis detection", hysteresis_detection, dim, dim));
    //options->addSlider("hysteresis low value", 0, 2.0, hysteresis_low, width-hSpace, dim);
    //options->addSlider("hysteresis high value", 0, 2.0, hysteresis_high, width-hSpace, dim);
    
    options->addSpacer(width-hSpace, 2);
    options->addWidgetDown(new ofxUIToggle("close options window", false, dim, dim));
    
    options->addSpacer(width-hSpace, 2);
    
    ofAddListener(options->newGUIEvent, this, &OMDebugView::guiEvent);
    
    
}

//--------------------------------------------------------------
void OMDebugView::guiEvent(ofxUIEventArgs &e) {
    
    ofApp* t = (ofApp*) ofGetAppPtr();
    string name = e.widget->getName();
    int id = e.widget->getID();
    string pname = e.widget->getParent()->getName();
    
    
    if(name == "Bookmark deactivation only with 2 Fingers") {
        
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
        bookmark_deactivate_twofingers = toggle->getValue();
        saveSettings();
    }
    if(name == "Bookmark activation sound") {
        
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
        bookmark_activation_sound = toggle->getValue();
        saveSettings();
    }
    if(name == "Bookmark pre sense sound") {
        
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
        bookmark_presense_sound = toggle->getValue();
        saveSettings();
    }
    if(name == "Touch hysteresis detection") {
        
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
        hysteresis_detection = toggle->getValue();
        saveSettings();
    }
    
    if(name == "hysteresis low value") {
        
        ofxUISlider *slider = (ofxUISlider *) e.widget;
        hysteresis_low = slider->getScaledValue();
        saveSettings();
    }
    if(name == "hysteresis high value") {
        
        ofxUISlider *slider = (ofxUISlider *) e.widget;
        hysteresis_high = slider->getScaledValue();
        saveSettings();
    }
    
    
    if(name == "Tighter gesture separation") {
        
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
        better_gestures = toggle->getValue();
        saveSettings();
    }
    
    if(name == "close options window") {
        
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
        toggle->setValue(false);
        pressKeyD();
    }
    
    //
//
//    if(name == "BACK TO DEFAULT") {
//        
//        ofxUISlider *slider = (ofxUISlider *) e.widget;
//        backToDefaultModeTime = slider->getScaledValue();
//        backToDefaultModeTimer.setup(backToDefaultModeTime, true);
//        saveSettings();
//    }

    
}

// ------------------------------------------------
void OMDebugView::update(ofEventArgs &e) {
    
    //if(useTUIO) tuio.getMessage();
    
    if(options->isVisible()) {
        ofApp* t = (ofApp*) ofGetAppPtr();

        
        // debug mode max 30 sec zeigen
        if(ofGetElapsedTimeMillis() - debugViewStartedTimestamp > 30000) {
            
            debug = false;
            options->setVisible(debug);
        }
    }

    if(ofGetElapsedTimeMillis()-keyPadStartedTimestamp > 8000) {
        
        keypadMode = false;
    };
    
}

// ------------------------------------------------
void OMDebugView::draw(ofEventArgs &e) {
    
    ofSetColor(255);
    if(keypadMode) tastenfeld.draw(0, 0);
}

// ------------------------------------------------
void OMDebugView::exit(ofEventArgs &e) {
    
    //if(useTUIO) stopTuio();
}

//--------------------------------------------------------------
void OMDebugView::saveSettings() {
    
    ofApp* t = (ofApp*) ofGetAppPtr();
    XML.setValue("DEBUG", false);

    XML.setValue("BOOKMARK_DEACTIVATE_TWOFINGERS", bookmark_deactivate_twofingers);
    XML.setValue("BOOKMARK_ACTIVATION_SOUND", bookmark_activation_sound);
    XML.setValue("BOOKMARK_PRESENSE_SOUND", bookmark_presense_sound);
    XML.setValue("HYSTERESIS_DETECTION", hysteresis_detection);
    XML.setValue("HYSTERESIS_LOW", hysteresis_low);
    XML.setValue("HYSTERESIS_HIGH", hysteresis_high);
    XML.setValue("BETTER_GESTURES", better_gestures);
 
//    XML.saveFile("settings.xml");
    XML.saveFile(ofxiPhoneGetDocumentsDirectory()+"settings.xml");
    
}

//--------------------------------------------------------------
void OMDebugView::keyPressed(ofKeyEventArgs &e){
    
}

//--------------------------------------------------------------
void OMDebugView::pressKeyD(){
    
    debugViewStartedTimestamp = ofGetElapsedTimeMillis();
    debug = !debug;
    options->setVisible(debug);
    saveSettings();
}


//--------------------------------------------------------------
void OMDebugView::pressed(ofMouseEventArgs &e){
    
    backToDefaultModeTimer.reset();
}

//--------------------------------------------------------------
void OMDebugView::dragged(ofMouseEventArgs &e){
    
    backToDefaultModeTimer.reset();
}

//--------------------------------------------------------------
void OMDebugView::released(ofMouseEventArgs &e){
    
    backToDefaultModeTimer.reset();
    
    bool clickTime = ofGetElapsedTimeMillis()-trippleClickTimestamp < 800;
    trippleClickTimestamp = ofGetElapsedTimeMillis();
    
    ofRectangle topLeft(0, 0, 200, 200);
    
    if(topLeft.inside(e.x, e.y)) {
        
        if(clickTime && trippleCount==2) {
            
            keypadMode = true;
            keyPadStartedTimestamp = ofGetElapsedTimeMillis();
            keys.clear();
            return;
        }
        else if(clickTime)trippleCount++;
        else trippleCount = 1;
    } else trippleCount = 0;
    
    // keypad
    if(keypadMode) {
        
        bool outside = false;
        bool ok_pressed = false;
        ofRectangle pad(44*2, 33*2, 388*2, 232*2);
        if(!pad.inside(e.x, e.y)) outside = true;
        float keyWidth = pad.width/4;
        float keyHeight = pad.height/3;
        int pressedX = (e.x-pad.x) / keyWidth;
        int pressedY = (e.y-pad.y) / keyHeight;
        if(pressedY==1) pressedX = (e.x-pad.x-keyWidth/2) / keyWidth;
        if(pressedY==2) pressedX--;
        int key = pressedY*4+pressedX;
        if(key==10) ok_pressed = true;
        
        if(outside) {
            keypadMode = false;
            //if(debug) ofNotifyKeyPressed('d'); // disable debug
            if(debug) pressKeyD(); // disable debug
        }
        else if(ok_pressed) {
            
            bool goodCode = true;
            if(keys.size()==CODE_LENGTH) {
                for(int i=0; i<keys.size(); i++) {
                    if(keys[i]!=keyCode[i]) goodCode = false;
                }
            }
            else goodCode = false;
            
            if(goodCode) {
                
                // right code entered
                //ofNotifyKeyPressed('d'); // enable debug mode
                pressKeyD(); // enable debug mode
            } else {
                if(debug) pressKeyD();; // disable debug
            }
            
            keypadMode = false;
        } else {
            keys.push_back(key);
        }
    }
}

