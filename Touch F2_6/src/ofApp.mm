//
// was ist refId ?
//
// was mit opacity?



#include "ofApp.h"



//--------------------------------------------------------------
void ofApp::setup(){

    #ifdef TARGET_OPENGLES
    ofSetOrientation(OF_ORIENTATION_90_LEFT);
    #endif
    
    // options
    
    XML.load(ofxiPhoneGetDocumentsDirectory() + "settings.xml");
    
    
    debugView.setup();
    drawTwoLines = true;
    
    backToDefaultModeTime = 120000;
    backToDefaultModeTimer.setup(backToDefaultModeTime, true);
    ofAddListener(backToDefaultModeTimer.TIMER_REACHED, this, &ofApp::backToDefaultEvent);
    
    klick.load("click_off.mp3");
    phone.load("katjaCalling.mp3");
    foundAFigureSound.load("blip2.mp3");
    
    navStarted.load("navstarted.mp3");
    welcomeHome.load("welcomeHomeActivated.mp3");
    phoneRinging.load("calling_sound.mp3");
    optionButtonKlack.load("windup.mp3");
    options_in.load("swish.mp3");
    
    // - - - - - - - - - - - - - - - - -
    //XML.loadFile("mySettings.xml");
    debug = true;
    omScalingTool::getScalingTool()->setup(2048, 1536);
    
    
    ofBackground(0,0,0);
    ofDisableAntiAliasing();
    
    //debugFont.load("fonts/Ubuntu-Regular.ttf", 24);
    
    Scene.setup();
    
    mov.load("movv2.mp4");
    mov.setLoopState(OF_LOOP_NORMAL);
    Scene.mov = &mov;
    
    drawMode = false;
    
    firstFingerIsDown = false;
    secondFingerIsDown  = false;
    finger1ReleasedTimeStamp = ofGetElapsedTimef();
    finger2ReleasedTimeStamp = ofGetElapsedTimef();
    
    // - - - - - - - - - - - - - - - - -

    blurAmount = 0;
    
    in_fbo.allocate(2048, 1536);
    out_fbo.allocate(2048, 1536);
    blur.setDownsampleScale(0.25);
    
    // - - - - - - - - - - - - - - - - -
    
	pointCount  = 0;
    pointCount1 = 0;
    pointCount2 = 0;

    //foundAFigureSound.play();
    
    draggingOptionsMenu = false;
    drawLines = true;
    selectedOption = -1;
    optionRect[0].set(1024, 0, 320, 290);
    optionRect[1].set(1024 +320, 0, 320, 290);
    optionRect[2].set(1024 +640, 0, 320, 290);
    optionRect[3].set(1024 -320, 0, 320, 290);
    optionRect[4].set(1024 -320*2, 0, 320, 290);
    optionRect[5].set(1024 -320*3, 0, 320, 290);
    
    shouldAnalyze = false;
    shouldAnalyseTimestamp = ofGetElapsedTimef();
}

//--------------------------------------------------------------
void ofApp::update(){
    
    float optMenuVal = Scene.overlayAnim.GetValue();
    if(optMenuVal>0.01 && !debugView.bookmark_deactivate_twofingers ) {
        
        blurAmount = ofMap(optMenuVal, 0, 1, 0, blurMax);
    }
    else if(drawMode) {
        if(blurAmount<blurMax) blurAmount += blurSpeedPerFrameIn;
        if(blurAmount>blurMax) blurAmount = blurMax;
    } else {
        
        if(blurAmount>0) blurAmount -= blurSpeedPerFrameOut;
        if(blurAmount<0) blurAmount = 0;
    }
    
    
    Scene.update();
    
    if(blurAmount>0) {
        
        ofSetColor(255);
        in_fbo.begin();
        ofClear(0, 0, 0, 0);
        drawScene();
        in_fbo.end();
        
        blur.setRadius(blurAmount);
        blur.process(in_fbo.getTextureReference(), out_fbo);
    }
    
    mov.update();
    
    if(debugView.hysteresis_detection) {
        
        if(shouldAnalyze && ofGetElapsedTimef() - shouldAnalyseTimestamp > 0.4) {
            
            bool foundFigure = analyseStroke();
            if(foundFigure) strokes.clear();
            //if(foundFigure) return;
            shouldAnalyze = false;
        }
    }
}


//--------------------------------------------------------------
float getAngle(ofPoint v1, ofPoint v2) {
    
    float dot = v1.x*v2.x + v1.y*v2.y;      // dot product
    float det = v1.x*v2.y - v1.y*v2.x;      //# determinant
    float angleRadians = atan2(det, dot);  // # atan2(y, x) or atan2(sin, cos)
    
    float angleDegrees = (angleRadians/PI)*180;
    return angleDegrees;
}

//--------------------------------------------------------------
void ofApp::drawScene(){
    
    
    Scene.draw();
    
    // drawLine
    if(blurAmount>0 && drawLines) {
        
        if(drawTwoLines) {
            
            // two lines
            ofPolyline drawLine1;
            ofPolyline drawLine2;
            float alpha = ofMap(blurAmount, blurMax, blurMax/4, 255, 0, true);
            
            for(int i = 0; i < pointCount1; i++){
                drawLine1.addVertex(dragPts1[i].x, dragPts1[i].y);
            }
            for(int i = 0; i < pointCount2; i++){
                drawLine2.addVertex(dragPts2[i].x, dragPts2[i].y);
            }
            
            drawLine1 = drawLine1.getResampledBySpacing(drawingLength);
            drawLine2 = drawLine2.getResampledBySpacing(drawingLength);
            vector<ofPoint> lineVertices1 = drawLine1.getVertices();
            vector<ofPoint> lineVertices2 = drawLine2.getVertices();
            float rad = drawingMaxRadius;
            for(int i=(int)lineVertices1.size()-1; i>=0; i--) {
                
                ofSetColor(255, 180, 0, alpha);
                ofDrawCircle(lineVertices1[i], rad);
                rad = MAX(0, rad-drawingFalloff);
            }
            rad = drawingMaxRadius;
            for(int i=(int)lineVertices2.size()-1; i>=0; i--) {
                
                ofSetColor(255, 180, 0, alpha);
                ofDrawCircle(lineVertices2[i], rad);
                rad = MAX(0, rad-drawingFalloff);
            }
            
//            for(int i = 0; i<(int) drawStrokes.size(); i++) {
//                
//                if(strokes[i].type == POINT) {
//                    
//                    ofSetColor(255, 180, 0, alpha);
//                    ofDrawCircle(strokes[i].pos, drawingMaxRadius);
//                };
//            }
            
        } else {
            
            // one line
            ofPolyline drawLine;
            float alpha = ofMap(blurAmount, blurMax, blurMax/4, 255, 0, true);
            
            for(int i = 0; i < pointCount; i++){
                drawLine.addVertex(dragPts[i].x, dragPts[i].y);
            }
            
            drawLine = drawLine.getResampledBySpacing(drawingLength);
            vector<ofPoint> lineVertices = drawLine.getVertices();
            float rad = drawingMaxRadius;
            for(int i=(int)lineVertices.size()-1; i>=0; i--) {
                
                ofSetColor(255, 180, 0, alpha);
                ofDrawCircle(lineVertices[i], rad);
                rad = MAX(0, rad-drawingFalloff);
            }
            
            for(int i = 0; i<(int) strokes.size(); i++) {
                
                if(strokes[i].type == POINT) {
                    
                    ofSetColor(255, 180, 0, alpha);
                    ofDrawCircle(strokes[i].pos, drawingMaxRadius);
                };
            }
        }
    }
    
}

//--------------------------------------------------------------
void ofApp::drawDebug(){
    
    if(debug) {
        
        // draw instructions
        ofSetColor(255,255,0, 220);
        string info = "D - debug\n";
        info += "F - fullscreen\n";
        info += "LEFT/RIGHT - switch comps\n";
        info += "ESC - quit\n";
        info += "\nfps "+ofToString(ofGetFrameRate(),2)+"\n";
        info += Scene.compNames[Scene.currentComp];
        //debugFont.drawString(info, 40, 160);
        
        //omScalingTool::getScalingTool()->push();
        
        
        // draw line angles
        for(int i = 0; i < lineVertices.size(); i++){
            if(i<lineVertices.size()) {
                
                float angle = lineAngles[i];
                ofDrawSphere(lineVertices[i], 4);
                ofSetColor(0, 255, 255, 255);
                if(abs(angle)>minCornerAngle)
                    ofDrawBitmapString(ofToString( angle,2), lineVertices[i].x, lineVertices[i].y);
            }
        }
        
        // draw lines
        for(int i = 0; i < lines.size(); i++){
            
            if(i%2==0) {
                if(lineType[i]==WINDING_RIGHT) ofSetColor(0, 0, 255);
                else if(lineType[i]==WINDING_LEFT) ofSetColor(0, 255, 0);
                else ofSetColor(255,150,0);
            } else {
                if(lineType[i]==WINDING_RIGHT) ofSetColor(0, 255, 255);
                else if(lineType[i]==WINDING_LEFT) ofSetColor(100, 255, 0);
                else ofSetColor(255,0,0);
            }
            lines[i].draw();
            
        }
        
        
        // dram anglesum
        int numV = lineVertices.size();
        if(numV>0) {
            
            ofSetColor(255,100,0);
            ofDrawBitmapString(ofToString( angleSum,2), lineVertices[numV-1].x+20, lineVertices[numV-1].y-40);
            ofDrawBitmapString(ofToString( lengthSum,2), lineVertices[numV-1].x+20, lineVertices[numV-1].y-20);
        }
        
        //omScalingTool::getScalingTool()->pop();
    }
    
}

//--------------------------------------------------------------
void ofApp::draw(){

    omScalingTool::getScalingTool()->push();
    //ofPushMatrix();
    //ofScale(0.45, 0.45);
    
    if(blurAmount>0) {
        
        ofSetColor(255);
        out_fbo.draw(0, 0);
    }
    else {
        
        drawScene();
    }
    drawDebug();
    
     Scene.drawOverlay();
    
    //ofPopMatrix();
    omScalingTool::getScalingTool()->pop();
}

//--------------------------------------------------------------
void ofApp::keyPressed  (int key){

    
    if(key == 's'){
        
        //XML.saveFile("mySettings.xml");
    }
    if(key == 'd'){
        
        debug = !debug;
    }
    if(key == 'f'){
        
        ofToggleFullscreen();
    }
    if(key == OF_KEY_LEFT) {
        
        int numComps = Scene.compNames.size()-1;
        Scene.currentComp--;
        if(Scene.currentComp<0) Scene.currentComp = numComps;
        Scene.showComp(Scene.currentComp);
    }
    if(key == OF_KEY_RIGHT) {
        
        int numComps = Scene.compNames.size();
        Scene.currentComp++;
        Scene.currentComp %= numComps;
        Scene.showComp(Scene.currentComp);
    }
    
//    if (key == '1')
//        blur.setDownsampleScale(1);
//    if (key == '2')
//        blur.setDownsampleScale(0.5);
//    if (key == '3')
//        blur.setDownsampleScale(0.25);
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}



//--------------------------------------------------------------
void ofApp::touchDown(ofTouchEventArgs & touch){
    
    backToDefaultModeTimer.reset();
    if(touch.id == 0)  firstFingerIsDown = true;
    if(touch.id == 1)  secondFingerIsDown = true;
    
    ofPoint scaledMouse = omScalingTool::getScalingTool()->scaleMouse(touch.x, touch.y);
    
    //We start a new stroke
    
    pointCount	= 0;
    pointCount1 = 0;
    pointCount2 = 0;
    shouldAnalyze = false;
    cutStroke();
    
    
    if(touch.id == 0) {
    
        pressedLocationF1.set(scaledMouse);
        movedLocationF1 = pressedLocationF1;
        travelledDistance = 0;
    }
    if(touch.id == 1) {
        
        pressedLocationF2.set(scaledMouse);
        movedLocationF2 = pressedLocationF2;
        travelledDistance = 0;
        
        ofPoint middle = (pressedLocationF2 - pressedLocationF1)*0.5;
        pressedLocation = pressedLocationF1 +middle;
    }
    
    bool twoFingerMode = firstFingerIsDown && secondFingerIsDown;
    
    ofRectangle upperBorder, lowerBorder;
    upperBorder.set(0, 0, 2048, 30);
    lowerBorder.set(0, 290-100, 2048, 200);
    
    
    if(twoFingerMode && upperBorder.inside(scaledMouse)) {
        
        draggingOptionsMenu = true;
        drawLines = false;
    }
    else if(Scene.overlayAnim.GetValue()>0.5 && lowerBorder.inside(scaledMouse)) {
        
        draggingOptionsMenu = true;
        drawLines = false;
    }
    else if(twoFingerMode && Scene.drawingRect.inside(scaledMouse)) {
        
        drawMode = true;
        drawLines = true;
    }
    
    // check opt buttons
    int oldSelected = -1;
    clickedIntoOption = -1;
    for(int i=0; i<6; i++) {
        
        if(Scene.overlayAnim.GetValue()>0.9 && optionRect[i].inside(scaledMouse)) {
            
            selectedOption = i;
            clickedIntoOption = i;
        }
        if(selectedOption==2) Scene.showOverlay("07_Drop_Down_Menu_6");
        else if(selectedOption==1) Scene.showOverlay("07_Drop_Down_Menu_5");
        else if(selectedOption==0) Scene.showOverlay("07_Drop_Down_Menu_4");
        else Scene.showOverlay("07_Drop_Down_Menu");
    }
    
    if(Scene.overlayAnim.GetValue()>0.9 && optionRect[0].inside(scaledMouse)) {
        
        Scene.elementColor[1005].set(180);
    } else Scene.elementColor[1005].set(255);
    
    if(Scene.overlayAnim.GetValue()>0.9 && optionRect[1].inside(scaledMouse)) {
        
        Scene.elementColor[1006].set(180);
    } else Scene.elementColor[1006].set(255);
    if(Scene.overlayAnim.GetValue()>0.9 && optionRect[2].inside(scaledMouse)) {
        Scene.elementColor[1007].set(180);
    } else Scene.elementColor[1007].set(255);
    
    //if(!twoFingerMode) checkNaviButtons(scaledMouse, false);
}

//--------------------------------------------------------------
void ofApp::touchMoved(ofTouchEventArgs & touch){
    
    backToDefaultModeTimer.reset();
    ofPoint scaledMouse = omScalingTool::getScalingTool()->scaleMouse(touch.x, touch.y);
    
    
    if(draggingOptionsMenu) {
        
        Scene.showOverlay("07_Drop_Down_Menu");
        float animTarget = ofMap(scaledMouse.y, 0, 290, 0, 1, true);
        Scene.animateOverlay(animTarget, 0.1);
    }
    // check opt buttons
    int oldSelected = selectedOption;
    for(int i=0; i<6; i++) {
        
        if(Scene.overlayAnim.GetValue()>0.9 && optionRect[i].inside(scaledMouse)) {
            
            selectedOption = i;
            clickedIntoOption = i;
        }
        }
    if(oldSelected!=selectedOption && selectedOption>=0) {
        
        if(selectedOption==2) Scene.showOverlay("07_Drop_Down_Menu_6");
        else if(selectedOption==1) Scene.showOverlay("07_Drop_Down_Menu_5");
        else if(selectedOption==0) Scene.showOverlay("07_Drop_Down_Menu_4");
        else Scene.showOverlay("07_Drop_Down_Menu");
    }
    if(oldSelected!=selectedOption && debugView.bookmark_presense_sound && selectedOption>=0) {
        
        optionButtonKlack.play();
    }
    
    
    if(touch.id == 0) {
        
        movedLocationF1.set(scaledMouse);
    }
    if(touch.id == 1) {
        
        movedLocationF2.set(scaledMouse);
        
        ofPoint middle = (movedLocationF2 - movedLocationF1)*0.5;
        ofPoint releasedPoint = movedLocationF1 +middle;
        
        // sum up travelling for detecting draw point
        travelledDistance += (releasedPoint-pressedLocation).length();
        pressedLocation = releasedPoint;
    }
    
    bool twoFingerMode = firstFingerIsDown && secondFingerIsDown;
    if(!twoFingerMode) return;
    
    
    
    
    
    
    // add point

        
        if(touch.id==0 && pointCount1 < NUM_PTS -1){
            
            dragPts1[pointCount1].set(scaledMouse);
            pointCount1++;
        }
        if(touch.id==1 && pointCount2 < NUM_PTS -1){
            
            dragPts2[pointCount2].set(scaledMouse);
            pointCount2++;
        }
    
    if(touch.id==0 && pointCount < NUM_PTS -1){
        
        dragPts[pointCount].set(pressedLocation);
        pointCount++;
    }
    
    
    // find lines and bows
    //cutStroke();
    


}

//--------------------------------------------------------------
void ofApp::touchUp(ofTouchEventArgs & touch){
    
    
    backToDefaultModeTimer.reset();
    if(touch.id == 0)  firstFingerIsDown = false;
    if(touch.id == 1)  secondFingerIsDown = false;
    
    bool finishedDrawing = drawMode;
    drawMode = false;

    
    ofPoint scaledMouse = omScalingTool::getScalingTool()->scaleMouse(touch.x, touch.y);
    
    
    
    if(!draggingOptionsMenu && scaledMouse.y > 290) {
        
        if(!debugView.bookmark_deactivate_twofingers) Scene.animateOverlay(0);
    }
    else if(draggingOptionsMenu && scaledMouse.y > 290/2) {
        
        Scene.animateOverlay(1);
        if(debugView.bookmark_activation_sound && !options_in.isPlaying()) options_in.play();
    }
    else if(draggingOptionsMenu && scaledMouse.y < 290/2) {
        
        Scene.animateOverlay(0);
    }
    if(!firstFingerIsDown && !secondFingerIsDown) draggingOptionsMenu = false;
    
    if(touch.id == 0) {
        
        movedLocationF1.set(scaledMouse);
        finger1ReleasedTimeStamp = ofGetElapsedTimef();
    }
    if(touch.id == 1) {
        
        movedLocationF2.set(scaledMouse);
        finger2ReleasedTimeStamp = ofGetElapsedTimef();
        
        ofPoint middle = (movedLocationF2 - movedLocationF1)*0.5;
        ofPoint releasedPoint = movedLocationF1 +middle;
        
        // sum up travelling for detecting draw point
        travelledDistance += (releasedPoint-pressedLocation).length();
        pressedLocation = releasedPoint;
        
        // add point
        if(pointCount < NUM_PTS -1){
            
            dragPts[pointCount].set(pressedLocation);
            pointCount++;
        }
    }
    //if(touch.id > 0) return;
    

    
    // detect drawing point
    ofPoint releasedPoint(pressedLocation);
    float timediff = ABS(finger2ReleasedTimeStamp-finger1ReleasedTimeStamp);
    //cout << "timediff : " << timediff << endl;
    if(travelledDistance < maxPointDistance && timediff<0.15) {
        
        stroke newPoint;
        newPoint.id = 0;
        newPoint.type = POINT;
        newPoint.pos = releasedPoint;
        newPoint.angle = 0;
        newPoint.angleTolerance = 0;
        newPoint.length = 0;
        newPoint.timestamp = ofGetElapsedTimef();
        strokes.push_back(newPoint);
    }
    
    if(!firstFingerIsDown && !secondFingerIsDown) {
    
        // touchup
        stroke newStroke;
        newStroke.id = 0;
        newStroke.type = TOUCHUP;
        newStroke.angle = 0;
        newStroke.angleTolerance = 0;
        newStroke.length = 0;
        newStroke.pos = releasedPoint;
        newStroke.timestamp = ofGetElapsedTimef();
        strokes.push_back(newStroke);
        
        
        cutStroke();
        bool foundFigure = analyseStroke();
        if(foundFigure) strokes.clear();
        if(foundFigure) return;
        shouldAnalyze = true;
        shouldAnalyseTimestamp = ofGetElapsedTimef();
    }

    
    
    
    Scene.elementColor[1005].set(255);
    Scene.elementColor[1006].set(255);
    Scene.elementColor[1007].set(255);
    // check opt buttons
    if(Scene.overlayAnim.GetValue()>0.9 && optionRect[0].inside(scaledMouse) && clickedIntoOption==0) {
        
        klick.play();
        Scene.showComp("01_navi_movie");
        mov.play();
        if(!debugView.bookmark_deactivate_twofingers) Scene.animateOverlay(0);
        phoneRinging.stop();
        phoneRinging.setPosition(0);
        Scene.showOverlay("07_Drop_Down_Menu_4");
        return;
    }
    if(Scene.overlayAnim.GetValue()>0.9 && optionRect[1].inside(scaledMouse)&& clickedIntoOption==1) {
        
        klick.play();
        if(!debugView.bookmark_deactivate_twofingers) Scene.animateOverlay(0);
        Scene.showComp("03_com_calling");
        phoneRinging.play();
        Scene.showOverlay("07_Drop_Down_Menu_5");
        return;
    }
    if(Scene.overlayAnim.GetValue()>0.9 && optionRect[2].inside(scaledMouse)&& clickedIntoOption==2) {
        
        klick.play();
        if(!debugView.bookmark_deactivate_twofingers) Scene.animateOverlay(0);
        Scene.showComp("06_lighted_home");
        phoneRinging.stop();
        phoneRinging.setPosition(0);
        Scene.showOverlay("07_Drop_Down_Menu_6");
        return;
    }
    if(Scene.overlayAnim.GetValue()>0.9 && scaledMouse.y<290) {
        return;
    }
    
    // check navi buttons
    checkNaviButtons(scaledMouse, finishedDrawing);
    
}

//--------------------------------------------------------------
void ofApp::checkNaviButtons(ofPoint p, bool finishedDrawing) {
    
    int releasedID = -1;
    for(int i=0; i<Scene.currentNavIds.size(); i++) {
        
        int id = Scene.currentNavIds[i];
        if(Scene.buttonRectangle[id].inside(p)) releasedID = id;
    }
    // button press
    if(releasedID>=0 && !finishedDrawing) {
        
        int oldComp = Scene.currentComp;
        Scene.showComp(Scene.buttonTargets[releasedID]);
        //strokes.clear();
        
        if(Scene.compNames[Scene.currentComp]=="01_navi_movie" || Scene.compNames[Scene.currentComp]=="01_navi_movie_submenu") {
            
            mov.play();
        } else {
            
            mov.stop();
            mov.setPosition(0);
        }
        
        if(Scene.compNames[Scene.currentComp]=="03_com_calling") {
            
            phoneRinging.play();
        } else {
            
            phoneRinging.stop();
            phoneRinging.setPosition(0);
        }
        
        if(oldComp!=Scene.currentComp) klick.play();
    }
}

//--------------------------------------------------------------
void ofApp::backToDefaultEvent(ofEventArgs &e) {
    
    Scene.showComp("00_home");
    mov.stop();
    mov.setPosition(0);
    phoneRinging.stop();
    phoneRinging.setPosition(0);
    draggingOptionsMenu = false;
    Scene.animateOverlay(0);
}

//--------------------------------------------------------------
void ofApp::touchDoubleTap(ofTouchEventArgs & touch){
    
}

//--------------------------------------------------------------
void ofApp::touchCancelled(ofTouchEventArgs & touch){
    
}

//--------------------------------------------------------------
void ofApp::lostFocus(){
    
}

//--------------------------------------------------------------
void ofApp::gotFocus(){
    
}

//--------------------------------------------------------------
void ofApp::gotMemoryWarning(){
    
}

//--------------------------------------------------------------
void ofApp::deviceOrientationChanged(int newOrientation){
    
}




//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

#pragma mark
//--------------------------------------------------------------
void ofApp::cutStroke(){
    
    //cout << "  - cut stroke" << endl;
    
    float minWindingAngle = old_minWindingAngle;
    float minWindingChangeAngle = old_minWindingChangeAngle;
    float minArcLength = old_minArcLength;
    float minArcAngleSum = old_minArcAngleSum;
    float maxArcAngleSum = old_maxArcAngleSum;
    
    if(debugView.better_gestures) {
        
         minWindingAngle = new_minWindingAngle;
         minWindingChangeAngle = new_minWindingChangeAngle;
         minArcLength = new_minArcLength;
         minArcAngleSum = new_minArcAngleSum;
         maxArcAngleSum = new_maxArcAngleSum;
    }
    
    lines.clear();
    lineType.clear();

    angleSum = 0;
    angleSignedSum = 0;
    lengthSum = 0;
    ofPolyline line;
    
    for(int i = 0; i < pointCount; i++){
        line.addVertex(dragPts[i].x, dragPts[i].y);
    }
    
    line = line.getResampledBySpacing(sampleSpacingLength);
    
    lineVertices =  line.getVertices();
    int verticesNum = lineVertices.size();
    lineAngles.resize(verticesNum);
    
    
    // cut the line into pieces by checking if the winding changes
    vector <ofPoint> currentLine;
    
    if(verticesNum>1) {
        
        lineAngles[0] = 0;
        lineAngles[1] = 0;
        
        currentLine.push_back(lineVertices[0]);
        currentLine.push_back(lineVertices[1]);
    }

    int winding = 0; // not set
    int newWinding = 0;
    
    for(int i = 2; i < verticesNum; i++){
        
        ofPoint v1 = lineVertices[i-1] - lineVertices[i-2];
        ofPoint v2 = lineVertices[i] - lineVertices[i-1];
        
        lineAngles[i] = getAngle(v1, v2);
        
        angleSignedSum += lineAngles[i];
        angleSum += abs(lineAngles[i]);
        lengthSum += v1.length();

        
        if(winding==99) {
            
            // first winding
            winding = 0;
            if(lineAngles[i] > minWindingAngle) winding = WINDING_RIGHT;
            if(lineAngles[i] < -minWindingAngle)  winding = WINDING_LEFT;

            currentLine.push_back(lineVertices[i]);
            
        } else {
            

            if(lineAngles[i] > minWindingAngle) newWinding = WINDING_RIGHT;
            if(lineAngles[i] < -minWindingAngle) newWinding = WINDING_LEFT;
            
            bool switch1 = winding==WINDING_RIGHT && lineAngles[i] < minWindingChangeAngle;
            bool switch2 = winding==WINDING_LEFT && lineAngles[i] > -minWindingChangeAngle;
            bool corner = abs(lineAngles[i])>minCornerAngle;
            
            
            if(corner || switch1 || switch2)
            {
                // corner
                bool itsALine = abs(angleSignedSum) < maxAngleTotalLineBias && lengthSum>minLineLength;
                bool itsACurve = lengthSum>minArcLength && abs(angleSignedSum) > minArcAngleSum;
                
                if(itsALine) {
                    
                    cout << " line1 " << angleSignedSum << endl;
                    lines.push_back(currentLine);
                    lineType.push_back(STRAIGHT);
                }
                else if(itsACurve) {
                    
                    lines.push_back(currentLine);
                    if(winding==WINDING_RIGHT) lineType.push_back(WINDING_RIGHT);
                    else if(winding==WINDING_LEFT) lineType.push_back(WINDING_LEFT);
                    else {
                        
                        cout << " line1x " << angleSignedSum << endl;
                       lineType.push_back(STRAIGHT);
                    }
                }
                if(corner) {
                    
                    stroke newPoint;
                    newPoint.id = 0;
                    newPoint.type = CORNER;
                    newPoint.pos = ofPoint(0,0);
                    newPoint.angle = 0;
                    newPoint.angleTolerance = 0;
                    newPoint.length = 0;
                    newPoint.timestamp = ofGetElapsedTimef();
                    strokes.push_back(newPoint);
                }
                
                
                // new line
                currentLine.clear();
                currentLine.push_back(lineVertices[i-1]);
                currentLine.push_back(lineVertices[i]);
                newWinding = 0;
                angleSum = 0;
                angleSignedSum = 0;
                lengthSum = 0;
            }
            else if(winding==0 && newWinding!=0)
            {
                // straight line ends
                bool itsALine = lengthSum>minLineLength;
                if(itsALine) {
                    
                    cout << " line2 " << angleSignedSum << endl;
                    lines.push_back(currentLine);
                    lineType.push_back(STRAIGHT);
                }
                
                // new line
                currentLine.clear();
                currentLine.push_back(lineVertices[i-1]);
                currentLine.push_back(lineVertices[i]);
                angleSum = 0;
                angleSignedSum = 0;
                lengthSum = 0;
            }
            
            else if(newWinding != winding)
            {
                
                // winding changes
                bool itsALine = abs(angleSignedSum) < maxAngleTotalLineBias && lengthSum>minLineLength;
                bool itsACurve = lengthSum>minArcLength && abs(angleSignedSum) > minArcAngleSum;
                
                if(itsALine) {
                    
                    cout << " line3 " << angleSignedSum << endl;
                    lines.push_back(currentLine);
                    lineType.push_back(STRAIGHT);
                }
                else if(itsACurve) {
                    
                    lines.push_back(currentLine);
                    if(winding==WINDING_RIGHT) lineType.push_back(WINDING_RIGHT);
                    else if(winding==WINDING_LEFT) lineType.push_back(WINDING_LEFT);
                    else {
                        
                        cout << " line3x " << angleSignedSum << endl;
                        lineType.push_back(STRAIGHT);
                    }
                }
                
                // new line
                currentLine.clear();
                currentLine.push_back(lineVertices[i-1]);
                currentLine.push_back(lineVertices[i]);
                newWinding = 0;
                angleSum = 0;
                angleSignedSum = 0;
                lengthSum = 0;
            }
            else if(winding!=0 && angleSum > maxArcAngleSum )
            {
                
                // winding changes
                bool itsALine = abs(angleSignedSum) < maxAngleTotalLineBias && lengthSum>minLineLength;
                bool itsACurve = lengthSum>minArcLength && abs(angleSignedSum) > minArcAngleSum;
                
                if(itsALine) {
                    
                    cout << " line4 " << angleSignedSum << endl;
                    lines.push_back(currentLine);
                    lineType.push_back(STRAIGHT);
                }
                else if(itsACurve) {
                    
                    lines.push_back(currentLine);
                    if(winding==WINDING_RIGHT) lineType.push_back(WINDING_RIGHT);
                    else if(winding==WINDING_LEFT) lineType.push_back(WINDING_LEFT);
                    else {
                        
                        cout << " line4x " << angleSignedSum << endl;
                       lineType.push_back(STRAIGHT);
                    }
                }
                
                // new line
                currentLine.clear();
                currentLine.push_back(lineVertices[i-1]);
                currentLine.push_back(lineVertices[i]);
                newWinding = 0;
                angleSum = 0;
                angleSignedSum = 0;
                lengthSum = 0;
            }
            
            else {
                
                // winding continues
                currentLine.push_back(lineVertices[i]);
            }
            winding = newWinding;
        }
    }

    // close last line
    if(currentLine.size()>0) {
        
        bool itsALine = abs(angleSignedSum) < maxAngleTotalLineBias && lengthSum>minLineLength;
        bool itsACurve = lengthSum>minArcLength && abs(angleSignedSum) > minArcAngleSum;
        
        if(itsALine) {
            
            cout << " line5 " << angleSignedSum << endl;
            lines.push_back(currentLine);
            lineType.push_back(STRAIGHT);
        }
        else if(itsACurve) {
            
            lines.push_back(currentLine);
            if(winding==WINDING_RIGHT) lineType.push_back(WINDING_RIGHT);
            else if(winding==WINDING_LEFT) lineType.push_back(WINDING_LEFT);
            else {
                
                cout << " line6 " << angleSignedSum << endl;
                lineType.push_back(STRAIGHT);
            }
        }
        
        currentLine.clear();
    }
    
    
}
#pragma mark

//--------------------------------------------------------------
bool ofApp::analyseStroke(){

    bool foundAFigure = false;
    
    for(int i = (int) strokes.size()-1; i>=0  ; i--) {
        
        if(ofGetElapsedTimef() - strokes[i].timestamp > strokeTimeOut) {
            
            strokes.erase(strokes.begin()+i);
        }
    }
    
    for(int i = 0; i < (int) lines.size(); i++){
        
        vector<ofPoint> points = lines[i].getVertices();
        ofPoint start = points[0];
        ofPoint end = points[(int)points.size()-1];
        ofPoint direction = end - start;
        
        stroke newStroke;
        newStroke.id = i;
        newStroke.type = lineType[i];
        newStroke.angle = 0;
        newStroke.angleTolerance = 0;
        newStroke.length = direction.length();
        newStroke.pos = start + (end-start)*0.5;
        
        if(newStroke.type == STRAIGHT) {
            
            ofPoint null(0, -1);
            float angle = getAngle(null, direction);
            if(angle < 0) angle += 360;
            newStroke.angle = angle;
        }
        
        newStroke.timestamp = ofGetElapsedTimef();
        strokes.push_back(newStroke);
    }
    
    if(debugView.hysteresis_detection) {
        
        if(!shouldAnalyze) return false;
        if(ofGetElapsedTimef() - shouldAnalyseTimestamp <= 0.4) return false;
    }
    
    
    cout << "analyse: ";
    for(int i = 0; i<(int) strokes.size()  ; i++) {
        
        //cout << strokes[i].id;
        string name = "line";
        if(strokes[i].type==WINDING_RIGHT) name = "bowR";
        if(strokes[i].type==WINDING_LEFT) name = "bowL";
        if(strokes[i].type==POINT) name = "point";
        if(strokes[i].type==TOUCHUP) name = "touchup";
        if(strokes[i].type==CORNER) name = "corner";
        cout << name;
        //if(strokes[i].type==STRAIGHT) cout << strokes[i].angle;
        cout << strokes[i].length;
        if(i<(int) strokes.size()-1) cout << ", ";
    }
    cout << endl;
    
    
    
    // now check the figures
    
    // heart constrains - - - - - - - - - - -
    
    // from left to right in one stroke
    bool foundHeart = false;
    
    stroke heartLine1;
    heartLine1.type = STRAIGHT;
    heartLine1.angle = 315;
    heartLine1.angleTolerance = 40;
    heartLine1.found = false;
    heartLine1.id = -1;
    heartLine1.pos = ofPoint(-1, -1);
    
    stroke heartLine2;
    heartLine2.type = STRAIGHT;
    heartLine2.angle = 225;
    heartLine2.angleTolerance = 40;
    heartLine2.found = false;
    heartLine2.id = -1;
    heartLine2.pos = ofPoint(-1, -1);
    
    stroke heartBow1;
    heartBow1.type = WINDING_RIGHT;
    heartBow1.found = false;
    heartBow1.id = -1;
    heartBow1.pos = ofPoint(-1, -1);
    
    // heartLine1.id < heartBow1.id
    // heartBow1.id < heartLine2.id
    // heartLine1.pos.x < heartLine2.pos.x
    // heartLine1.pos.y < heartBow1.pos.y
    // heartLine2.pos.y < heartBow1.pos.y
    
    // from right to left in one stroke
    bool foundHeart_a1 = false;
    
    stroke heartLine1_a1;
    heartLine1_a1.type = STRAIGHT;
    heartLine1_a1.angle = 45;
    heartLine1_a1.angleTolerance = 40;
    heartLine1_a1.found = false;
    heartLine1_a1.id = -1;
    heartLine1_a1.pos = ofPoint(-1, -1);
    
    stroke heartLine2_a1;
    heartLine2_a1.type = STRAIGHT;
    heartLine2_a1.angle = 135;
    heartLine2_a1.angleTolerance = 40;
    heartLine2_a1.found = false;
    heartLine2_a1.id = -1;
    heartLine2_a1.pos = ofPoint(-1, -1);
    
    stroke heartBow1_a1;
    heartBow1_a1.type = WINDING_LEFT;
    heartBow1_a1.found = false;
    heartBow1_a1.id = -1;
    heartBow1_a1.pos = ofPoint(-1, -1);
    

    // from center right and from center left
    bool foundHeart_a2 = false;
    
    stroke heartLine1_a2;
    heartLine1_a2.type = STRAIGHT;
    heartLine1_a2.angle = 225;
    heartLine1_a2.angleTolerance = 40;
    heartLine1_a2.found = false;
    heartLine1_a2.id = -1;
    heartLine1_a2.pos = ofPoint(-1, -1);
    
    stroke heartLine2_a2;
    heartLine2_a2.type = STRAIGHT;
    heartLine2_a2.angle = 135;
    heartLine2_a2.angleTolerance = 40;
    heartLine2_a2.found = false;
    heartLine2_a2.id = -1;
    heartLine2_a2.pos = ofPoint(-1, -1);
    
    stroke heartBow1_a2;
    heartBow1_a2.type = WINDING_RIGHT;
    heartBow1_a2.found = false;
    heartBow1_a2.id = -1;
    heartBow1_a2.pos = ofPoint(-1, -1);
    
    stroke heartBow2_a2;
    heartBow2_a2.type = WINDING_LEFT;
    heartBow2_a2.found = false;
    heartBow2_a2.id = -1;
    heartBow2_a2.pos = ofPoint(-1, -1);
    
    
    
    
    
    
    
    
    
    
    
    
    // zirkumflex constrains - - - - - - - - - - -
    
    // from left to right
    bool foundZirkumflex = false;
    
    stroke zirkumLine1;
    zirkumLine1.type = STRAIGHT;
    zirkumLine1.angle = 45;
    zirkumLine1.angleTolerance = 40;
    zirkumLine1.found = false;
    zirkumLine1.id = -1;
    zirkumLine1.pos = ofPoint(-1, -1);
    
    stroke zirkumLine2;
    zirkumLine2.type = STRAIGHT;
    zirkumLine2.angle = 135;
    zirkumLine2.angleTolerance = 40;
    zirkumLine2.found = false;
    zirkumLine2.id = -1;
    zirkumLine2.pos = ofPoint(-1, -1);
    
    // zirkumLine1.id < zirkumLine2.id
    // zirkumLine1.pos.x < zirkumLine2.pos.x
    
    // from right to left
    bool foundZirkumflex_a1 = false;
    
    stroke zirkumLine1_a1;
    zirkumLine1_a1.type = STRAIGHT;
    zirkumLine1_a1.angle = 315;
    zirkumLine1_a1.angleTolerance = 40;
    zirkumLine1_a1.found = false;
    zirkumLine1_a1.id = -1;
    zirkumLine1_a1.pos = ofPoint(-1, -1);
    
    stroke zirkumLine2_a1;
    zirkumLine2_a1.type = STRAIGHT;
    zirkumLine2_a1.angle = 225;
    zirkumLine2_a1.angleTolerance = 40;
    zirkumLine2_a1.found = false;
    zirkumLine2_a1.id = -1;
    zirkumLine2_a1.pos = ofPoint(-1, -1);
    
    
    
    
    
    
    
    
    
    
    
    
    // mark constrains - - - - - - - - - - -
    
    bool foundMark = false;
    
    stroke mark1;
    mark1.type = STRAIGHT;
    mark1.angle = 180;
    mark1.angleTolerance = 40;
    mark1.found = false;
    mark1.id = -1;
    mark1.pos = ofPoint(-1, -1);
    
    stroke mark2;
    mark2.type = POINT;
    mark2.angle = 0;
    mark2.angleTolerance = 0;
    mark2.found = false;
    mark2.id = -1;
    mark2.pos = ofPoint(-1, -1);
    
    bool foundMark_a1 = false;
    
    stroke mark1_a1;
    mark1_a1.type = STRAIGHT;
    mark1_a1.angle = 0;
    mark1_a1.angleTolerance = 40;
    mark1_a1.found = false;
    mark1_a1.id = -1;
    mark1_a1.pos = ofPoint(-1, -1);
    
    stroke mark2_a1;
    mark2_a1.type = POINT;
    mark2_a1.angle = 0;
    mark2_a1.angleTolerance = 0;
    mark2_a1.found = false;
    mark2_a1.id = -1;
    mark2_a1.pos = ofPoint(-1, -1);
    
    // mark1.id < mark2.id
    // mark1.pos.y < mark2.pos.y
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    // search for heart
    
    for(int i = 0; i<(int) strokes.size(); i++) {
        
        bool typeMatch1 = strokes[i].type == heartLine1.type;
        bool angleMatch1 = strokes[i].angle > heartLine1.angle -heartLine1.angleTolerance && strokes[i].angle < heartLine1.angle +heartLine1.angleTolerance;
        if(typeMatch1 && angleMatch1) {
            
            heartLine1.found = true;
            heartLine1.id = i;
            heartLine1.pos = strokes[i].pos;
        }
        
        bool typeMatch2 = strokes[i].type == heartLine2.type;
        bool angleMatch2 = strokes[i].angle > heartLine2.angle -heartLine2.angleTolerance && strokes[i].angle < heartLine2.angle +heartLine2.angleTolerance;
        if(typeMatch2 && angleMatch2) {
            
            heartLine2.found = true;
            heartLine2.id = i;
            heartLine2.pos = strokes[i].pos;
        }
        
        bool typeMatch3 = strokes[i].type == heartBow1.type;
        if(typeMatch3) {
            
            heartBow1.found = true;
            heartBow1.id = i;
            heartBow1.pos = strokes[i].pos;
        }
    }
    
    if(heartLine1.found && heartLine2.found && heartBow1.found) {
        
        bool correctOrder1 = heartLine1.id < heartBow1.id;
        bool correctOrder2 = heartBow1.id < heartLine2.id;
        bool correctOrder = correctOrder1 && correctOrder2;
        bool correctPosition1 = heartLine1.pos.x < heartLine2.pos.x;
        bool correctPosition2 = heartLine1.pos.y > heartBow1.pos.y;
        bool correctPosition3 = heartLine2.pos.y > heartBow1.pos.y;
        bool correctPosition = correctPosition1 && correctPosition2 && correctPosition3;
        
        if(correctOrder && correctPosition) foundHeart = true;
    }
    
    // search for heart_a1
    
    for(int i = 0; i<(int) strokes.size(); i++) {
        
        bool typeMatch1 = strokes[i].type == heartLine1_a1.type;
        bool angleMatch1 = strokes[i].angle > heartLine1_a1.angle -heartLine1_a1.angleTolerance && strokes[i].angle < heartLine1_a1.angle +heartLine1_a1.angleTolerance;
        if(typeMatch1 && angleMatch1) {
            
            heartLine1_a1.found = true;
            heartLine1_a1.id = i;
            heartLine1_a1.pos = strokes[i].pos;
        }
        
        bool typeMatch2 = strokes[i].type == heartLine2_a1.type;
        bool angleMatch2 = strokes[i].angle > heartLine2_a1.angle -heartLine2_a1.angleTolerance && strokes[i].angle < heartLine2_a1.angle +heartLine2_a1.angleTolerance;
        if(typeMatch2 && angleMatch2) {
            
            heartLine2_a1.found = true;
            heartLine2_a1.id = i;
            heartLine2_a1.pos = strokes[i].pos;
        }
        
        bool typeMatch3 = strokes[i].type == heartBow1_a1.type;
        if(typeMatch3) {
            
            heartBow1_a1.found = true;
            heartBow1_a1.id = i;
            heartBow1_a1.pos = strokes[i].pos;
        }
    }
    
    if(heartLine1_a1.found && heartLine2_a1.found && heartBow1_a1.found) {
        
        bool correctOrder1 = heartLine1_a1.id < heartBow1_a1.id;
        bool correctOrder2 = heartBow1_a1.id < heartLine2_a1.id;
        bool correctOrder = correctOrder1 && correctOrder2;
        bool correctPosition1 = heartLine1_a1.pos.x > heartLine2_a1.pos.x;
        bool correctPosition2 = heartLine1_a1.pos.y > heartBow1_a1.pos.y;
        bool correctPosition3 = heartLine2_a1.pos.y > heartBow1_a1.pos.y;
        bool correctPosition = correctPosition1 && correctPosition2 && correctPosition3;
        
        if(correctOrder && correctPosition) foundHeart_a1 = true;
    }
    
    // search for heart_a3
    
    for(int i = 0; i<(int) strokes.size(); i++) {
        
        bool typeMatch1 = strokes[i].type == heartLine1_a2.type;
        bool angleMatch1 = strokes[i].angle > heartLine1_a2.angle -heartLine1_a2.angleTolerance && strokes[i].angle < heartLine1_a2.angle +heartLine1_a2.angleTolerance;
        if(typeMatch1 && angleMatch1) {
            
            heartLine1_a2.found = true;
            heartLine1_a2.id = i;
            heartLine1_a2.pos = strokes[i].pos;
        }
        
        bool typeMatch2 = strokes[i].type == heartLine2_a2.type;
        bool angleMatch2 = strokes[i].angle > heartLine2_a2.angle -heartLine2_a2.angleTolerance && strokes[i].angle < heartLine2_a2.angle +heartLine2_a2.angleTolerance;
        if(typeMatch2 && angleMatch2) {
            
            heartLine2_a2.found = true;
            heartLine2_a2.id = i;
            heartLine2_a2.pos = strokes[i].pos;
        }
        
        bool typeMatch3 = strokes[i].type == heartBow1_a2.type;
        if(typeMatch3) {
            
            heartBow1_a2.found = true;
            heartBow1_a2.id = i;
            heartBow1_a2.pos = strokes[i].pos;
        }
        
        bool typeMatch4 = strokes[i].type == heartBow2_a2.type;
        if(typeMatch4) {
            
            heartBow2_a2.found = true;
            heartBow2_a2.id = i;
            heartBow2_a2.pos = strokes[i].pos;
        }
    }
    
    if(heartLine1_a2.found && heartLine2_a2.found && heartBow1_a2.found && heartBow2_a2.found) {
        
        bool correctOrder1 = heartLine1_a2.id > heartBow1_a2.id;
        bool correctOrder2 = heartLine2_a2.id > heartBow2_a2.id;
        bool correctOrder = correctOrder1 && correctOrder2;
        //bool correctPosition1 = heartLine1_a2.pos.x > heartLine2_a2.pos.x;
        bool correctPosition1 = true;
        bool correctPosition2 = heartLine1_a2.pos.y > heartBow1_a2.pos.y;
        bool correctPosition3 = heartLine2_a2.pos.y > heartBow2_a2.pos.y;
        bool correctPosition = correctPosition1 && correctPosition2 && correctPosition3;
        
        if(correctOrder && correctPosition) foundHeart_a2 = true;
    }

    
    
    
    
    
    
    
    
    
    
    // search for zirkumflex
    
    for(int i = 0; i<(int) strokes.size(); i++) {
        
        bool typeMatch1 = strokes[i].type == zirkumLine1.type;
        bool angleMatch1 = strokes[i].angle > zirkumLine1.angle -zirkumLine1.angleTolerance && strokes[i].angle < zirkumLine1.angle +zirkumLine1.angleTolerance;
        if(typeMatch1 && angleMatch1) {
            zirkumLine1.found = true;
            zirkumLine1.id = i;
            zirkumLine1.pos = strokes[i].pos;
        }
        
        bool typeMatch2 = strokes[i].type == zirkumLine2.type;
        bool angleMatch2 = strokes[i].angle > zirkumLine2.angle -zirkumLine2.angleTolerance && strokes[i].angle < zirkumLine2.angle +zirkumLine2.angleTolerance;
        if(typeMatch2 && angleMatch2) {
            zirkumLine2.found = true;
            zirkumLine2.id = i;
            zirkumLine2.pos = strokes[i].pos;
        }
    }
    if(zirkumLine1.found && zirkumLine2.found) {
        
        bool correctOrder = zirkumLine2.id > zirkumLine1.id;
        bool correctPosition = zirkumLine2.pos.x > zirkumLine1.pos.x;
        if(correctOrder && correctPosition) foundZirkumflex = true;
    }
    
    // search for zirkumflex_a1
    
    for(int i = 0; i<(int) strokes.size(); i++) {
        
        bool typeMatch1 = strokes[i].type == zirkumLine1_a1.type;
        bool angleMatch1 = strokes[i].angle > zirkumLine1_a1.angle -zirkumLine1_a1.angleTolerance && strokes[i].angle < zirkumLine1_a1.angle +zirkumLine1_a1.angleTolerance;
        if(typeMatch1 && angleMatch1) {
            zirkumLine1_a1.found = true;
            zirkumLine1_a1.id = i;
            zirkumLine1_a1.pos = strokes[i].pos;
        }
        
        bool typeMatch2 = strokes[i].type == zirkumLine2_a1.type;
        bool angleMatch2 = strokes[i].angle > zirkumLine2_a1.angle -zirkumLine2_a1.angleTolerance && strokes[i].angle < zirkumLine2_a1.angle +zirkumLine2_a1.angleTolerance;
        if(typeMatch2 && angleMatch2) {
            zirkumLine2_a1.found = true;
            zirkumLine2_a1.id = i;
            zirkumLine2_a1.pos = strokes[i].pos;
        }
    }
    if(zirkumLine1_a1.found && zirkumLine2_a1.found) {
        
        bool correctOrder = zirkumLine2_a1.id > zirkumLine1_a1.id;
        bool correctPosition = zirkumLine2_a1.pos.x < zirkumLine1_a1.pos.x;
        if(correctOrder && correctPosition) foundZirkumflex = true;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    // search for mark
    
    for(int i = 0; i<(int) strokes.size(); i++) {
        
        bool typeMatch1 = strokes[i].type == mark1.type;
        bool angleMatch1 = strokes[i].angle > mark1.angle -mark1.angleTolerance && strokes[i].angle < mark1.angle +mark1.angleTolerance;
        if(typeMatch1 && angleMatch1) {
            
            mark1.found = true;
            mark1.id = i;
            mark1.pos = strokes[i].pos;
        }
        
        bool typeMatch2 = strokes[i].type == mark2.type;
        if(typeMatch2) {
            
            mark2.found = true;
            mark2.id = i;
            mark2.pos = strokes[i].pos;
        }
    }
    
    if(mark1.found && mark2.found) {
        
        bool correctOrder = mark1.id < mark2.id;
        bool correctPosition = mark1.pos.y < mark2.pos.y;
        if(correctOrder && correctPosition) foundMark = true;
    }
    
    for(int i = 0; i<(int) strokes.size(); i++) {
        
        bool typeMatch1 = strokes[i].type == mark1_a1.type;
        bool angleMatch1 = strokes[i].angle > 360-40 || strokes[i].angle < 40;
        if(typeMatch1 && angleMatch1) {
            
            mark1_a1.found = true;
            mark1_a1.id = i;
            mark1_a1.pos = strokes[i].pos;
        }
        
        bool typeMatch2 = strokes[i].type == mark2_a1.type;
        if(typeMatch2) {
            
            mark2_a1.found = true;
            mark2_a1.id = i;
            mark2_a1.pos = strokes[i].pos;
        }
    }
    
    if(mark1_a1.found && mark2_a1.found) {
        
        bool correctOrder = mark1_a1.id > mark2_a1.id;
        bool correctPosition = mark1_a1.pos.y < mark2_a1.pos.y;
        if(correctOrder && correctPosition) foundMark_a1 = true;
    }
    
    
    
    
    // search for circle
    // we wont two right bows min 400 lunth & no left bow
    // has no touchups
    bool foundCircle = false;
    bool onlyRightBows = true;
    int countBows = 0;
    int countTouchUps = 0;
    
    bool onlyLeftBows = true;
    int countLeftBows = 0;
    bool noCorner = true;
    
    if(debugView.better_gestures) {
        for(int i = 0; i<(int) strokes.size(); i++) {
            
            if(strokes[i].type==WINDING_LEFT) onlyRightBows = false;
            if(strokes[i].type==WINDING_RIGHT && strokes[i].length>200) countBows++;
            if(strokes[i].type==WINDING_RIGHT && strokes[i].length>400) countBows+=2;
            
            if(strokes[i].type==WINDING_RIGHT) onlyLeftBows = false;
            if(strokes[i].type==WINDING_LEFT && strokes[i].length>200) countLeftBows++;
            if(strokes[i].type==WINDING_LEFT && strokes[i].length>400) countLeftBows+=2;
            if(strokes[i].type==TOUCHUP) countTouchUps++;
            
            if(strokes[i].type==CORNER) noCorner = false;
        }
    } else {
        
        for(int i = 0; i<(int) strokes.size(); i++) {
            
            if(strokes[i].type==WINDING_LEFT) onlyRightBows = false;
            if(strokes[i].type==WINDING_RIGHT && strokes[i].length>300) countBows++;
            
            if(strokes[i].type==WINDING_RIGHT) onlyLeftBows = false;
            if(strokes[i].type==WINDING_LEFT && strokes[i].length>300) countLeftBows++;
            if(strokes[i].type==TOUCHUP) countTouchUps++;
            
            if(strokes[i].type==CORNER) noCorner = false;
        }
    }
    if(onlyRightBows && countBows>=2 && countTouchUps<=1) foundCircle = true;
    if(onlyLeftBows && countLeftBows>=2 && countTouchUps<=1) foundCircle = true;
    if(!noCorner) foundCircle = false;
    
    if(foundCircle) {
        
        Scene.showComp("06_lighted_home");
        Scene.showOverlay("07_Drop_Down_Menu_6");
        welcomeHome.play();
        foundAFigure = true;
        cout << "*** circle" << endl;
        phoneRinging.stop();
    }
    else if(foundHeart) {
        
        Scene.showComp("03_com_calling");
        Scene.showOverlay("07_Drop_Down_Menu_5");
        phone.play();
        phoneRinging.play();
        foundAFigure = true;
        cout << "*** Herz" << endl;
    }
    else if(foundHeart_a1) {
        
        Scene.showComp("03_com_calling");
        Scene.showOverlay("07_Drop_Down_Menu_5");
        phone.play();
        phoneRinging.play();
        foundAFigure = true;
        cout << "*** Herz_a1" << endl;
    }
    else if(foundHeart_a2) {
        
        Scene.showComp("03_com_calling");
        Scene.showOverlay("07_Drop_Down_Menu_5");
        phone.play();
        phoneRinging.play();
        foundAFigure = true;
        cout << "*** Herz_a2" << endl;
    }
    else if(foundZirkumflex || foundZirkumflex_a1) {
        
        Scene.showComp("01_navi_movie");
        Scene.showOverlay("07_Drop_Down_Menu_4");
        foundAFigure = true;
        phoneRinging.stop();
        cout << "*** Zirkumflex" << endl;
        if(Scene.compNames[Scene.currentComp]=="01_navi_movie") {
            
            mov.play();
            navStarted.play();
        } else {
            
            mov.stop();
            mov.setPosition(0);
        }
    }
    else if(foundMark || foundMark_a1) {
        
        //Scene.showComp("06_lighted_home");
        //welcomeHome.play();
        //foundAFigure = true;
        //cout << "*** MArk" << endl;
    }

    return foundAFigure;
}

