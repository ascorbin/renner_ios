//
//  scene.hpp
//  touch
//
//  Created by Owi Mahn on 27/05/16.
//
//

#pragma once

#include "ofMain.h"
#include "ofxXmlSettings.h"
#include "CNESmoothInterpolator.h"

#define JUSTIFICATION_LEFT      0
#define JUSTIFICATION_CENTER    1
#define JUSTIFICATION_RIGHT     2

class scene {
    
public:
    
    void setup();
    void update();
    void draw();
    
    void updateButtonStates();
    void drawButtonStates();
    void pressButton(int buttonID);
    void releaseButton(int buttonID);
    void releaseAllButtons();
    
    string footagePath;
    
    ofVideoPlayer* mov;
    
    // comps - - - - - - - - - - -
    
    int currentComp;
    void showComp(int compNo);
    void showComp(string compName);
    
    // layers - - - - - - - - - - -
    
    ofxXmlSettings layersXML;
    void parseLayers(string file);
    void getSubLayers(string basePath);
    vector<int> layerIds;
    map<int, string> imagesPath;
    map<int, ofImage> images;
    map<int, string> elementFontName;
    map<int, string> elementText;
    map<int, ofColor> elementColor;
    map<int, ofRectangle> imageRectangle;
    map<int, int> imageJustifications;
    
    vector <string> compNames;
    vector < vector <string> > compLayersIds;
    vector <int> currentIds;
    
    int depth;
    // < id, depth >
    map <int, int> depthList;
    list< pair<int, int> > currentRenderList;
    
    // overlay
    CNESmoothInterpolator overlayAnim;
    vector <int> currentOverlayIds;
    list< pair<int, int> > overlayRenderList;
    void showOverlay(int compNo);
    void showOverlay(string compName);
    void hideOverlay();
    void drawOverlay();
    void animateOverlay(float target, float speed = 0.25);
    
    
    // fonts - - - - - - - - - - - -
    
    vector <string> fontNames;
    map <string, ofTrueTypeFont> usedFonts;
    
    
    // navigation - - - - - - - - - - - -
    
    ofxXmlSettings naviXML;
    void parseNavigation(string file);
    vector <string> naviCompNames;
    int currentNavigation;
    vector < vector <int> > compButtonIds;
    vector <int> currentNavIds;
    ofRectangle drawingRect;
    
    map<int, ofRectangle> buttonRectangle;
    map<int, string> buttonTargets;
    map<int, int> buttonActiveImageID;
    map<int, float> buttonAlpha;
    map<int, bool> buttonActive;
};

