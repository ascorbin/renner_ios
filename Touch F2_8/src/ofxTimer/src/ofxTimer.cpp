/***********************************************************************
 
 Copyright (c) 2009, Todd Vanderlin, www.vanderlin.cc
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 ***********************************************************************/

#include "ofxTimer.h"

ofxTimer::ofxTimer() {
	
    running = false;
}

ofxTimer::~ofxTimer() {
	ofRemoveListener(ofEvents().update, this, &ofxTimer::update);
}

// ---------------------------------------

void ofxTimer::setup(float millSeconds, bool loopTimer) {
	
	count		= 0;
	bLoop		= loopTimer;
	bPauseTimer = false;
	
	//timer
	bStartTimer = true;
	delay		= millSeconds;	// mill seconds
	setupDelay	= delay;
	timer		= 0;
	timerStart	= 0;
    bTimerFinished = false;
    running = true;
	
	//events
	ofAddListener(ofEvents().update, this, &ofxTimer::update);
	
}

// ---------------------------------------
void ofxTimer::reset() {
	count = 0;
	timer = 0;
	timerStart = 0;
//	delay = setupDelay;
	bStartTimer = true;
	bTimerFinished = false;
    running = false;
}

// ---------------------------------------
void ofxTimer::loop(bool b) {
	bLoop = b;
}

// ---------------------------------------
void ofxTimer::update(ofEventArgs &e) {

	if(!bPauseTimer) {
		if(bStartTimer) {
			bStartTimer = false;
			timerStart  = ofGetElapsedTimef();
		}
		
		float time = ofGetElapsedTimef() - timerStart;
		time *= 1000.0;		
		if(time >= delay) {
			count ++;
			if(!bLoop){
				bPauseTimer = true;
				bTimerFinished = true;//TODO noch kein unterschied zu bPaused;
			}
			bStartTimer = true;
			static ofEventArgs timerEventArgs;
			ofNotifyEvent(TIMER_REACHED, timerEventArgs, this);
            running = false;
		}
	}
}

// ---------------------------------------
void ofxTimer::setTimer(float millSeconds) {
	delay = millSeconds;
}

void ofxTimer::startTimer() {
    bStartTimer = true;
	bPauseTimer = false;
    bTimerFinished = false;
    running = true;
}

void ofxTimer::stopTimer() {
	bPauseTimer = true;
    running = false;
}

bool ofxTimer::isTimerFinished(){
	return bTimerFinished;
}

bool ofxTimer::isRunning(){
    return running;
}

float ofxTimer::getMillSeconds() {
    float returnVal = (ofGetElapsedTimef() - timerStart) * 1000.0f;
    if(returnVal < delay) {
        return returnVal;
    }
    else return 0;
}
