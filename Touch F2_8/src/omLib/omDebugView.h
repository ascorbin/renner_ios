//
//  OMDebugView.h
//  comspot
//
//  Created by owi on 13.03.13.
//
//

#pragma once

#include "ofMain.h"
#include "ofxXmlSettings.h"
#include "ofxTimer.h"
#include "ofxUI.h"


#define CODE_LENGTH 4
static int keyCode[CODE_LENGTH] = {1, 7, 0, 2};


class OMDebugView{
public:
    

    void setup();
    void update(ofEventArgs &e);
    void draw(ofEventArgs &e);
    void exit(ofEventArgs &e);
    
    void keyPressed(ofKeyEventArgs &e);
    void pressKeyD();
    void pressed(ofMouseEventArgs &e);
    void dragged(ofMouseEventArgs &e);
    void released(ofMouseEventArgs &e);
    
    
    ofxXmlSettings  XML;
    
    // debug panel
    
    bool has_bookmark;
    bool bookmark_deactivate_twofingers;
    bool bookmark_activation_sound;
    bool bookmark_presense_sound;
    bool hysteresis_detection;
    float hysteresis_low, hysteresis_high;
    bool better_gestures;
    bool show_debug_strokes;
    
    // v4
    string currentVersion;
    bool can_loose_one_finger;
    bool checkmark_instead_of_circle;
    void setParamsForVersion(string v);
    
    
    // others & own
    
    bool debug;
    ofxTimer        backToDefaultModeTimer;
    void            saveSettings();

    unsigned long long  debugViewStartedTimestamp;
    
    // UI - - - - - - - - - - - - - - - - - - - - -
    
    ofxUICanvas  *options;
    ofxUIRadio *versions;
    
    void guiEvent(ofxUIEventArgs &e);

    
    // tastenfeld
    // tripple click
    ofImage tastenfeld;
    
    unsigned long long  trippleClickTimestamp;
    int             trippleCount;
    bool            keypadMode;
    vector <int>    keys;
    unsigned long long  keyPadStartedTimestamp;
    

};
