/////////////////////////////////////////////////////////////////////////////
// CNESmoothInterpolator.h : Interpolates a float value over time
//                           with smooth dampening
//
// (c) 2011 Ole Dittmann, Mad Hat
#pragma once


//class NENGINE_EXP CNESmoothInterpolator {
class CNESmoothInterpolator {
    float Value;
    float Time;          // Time stops at target and always resets to zero when a new target is set
    float Modulo;
    float SourceKey[4];  // sx, sy, dx, dy - dx should be positive, x axis is time axis and y axis is value
    float TargetKey[4];  // tx, ty, dx, dy - dx should be negative and tx > sx
    bool Valid;          // True if Value already has been calculated for current time

public:
    // Advanced modulo function handling negative values
    inline static float CyclicModulo(float Value, float Modulo) {
        if (Modulo == 0) return Value;
        Value = fmod(Value, Modulo);
        if (Value < 0) Value = Modulo + Value;
        return Value;
    }

    // Evaluate a 2D cubic bezier kurve by x (instead of t) and return y and (optionally) the tangent dx and dy
    static float EvalCubicBezierByX(float* k1, float* k2, float x, float* dxdy = NULL) {
        float ax, bx, cx;
        float ay, by, cy;
        float tSquared, tCubed;

        float t = (x - k1[0]) / (k2[0] - k1[0]);
        float tx, tdx;

        cx = 3.0f * k1[2];
        bx = 3.0f * (k2[0] + k2[2] - k1[0]) - 2.0f * cx;
        ax = k2[0] - k1[0] - cx - bx;

        cy = 3.0f * k1[3];
        by = 3.0f * (k2[1] + k2[3] - k1[1]) - 2.0f * cy;
        ay = k2[1] - k1[1] - cy - by;

        for (int i = 0; i < 10; i++) {
            tSquared = t * t;
            tCubed = tSquared * t;
            tx = ax * tCubed + bx * tSquared + cx * t + k1[0];
            tdx = 3.0f * ax * tSquared + 2.0f * bx * t + cx;
            if (tdx == 0) break;
            t = t - (tx - x) / tdx;
            if (fabs(tx - x) < 0.000001f) break;
            if (t > 1) t = 1;
            if (t < 0) t = 0;
        }

        tSquared = t * t;
        tCubed = tSquared * t;
        if (dxdy != NULL) {
            dxdy[0] = 3.0f * ax * tSquared + 2.0f * bx * t + cx;
            dxdy[1] = 3.0f * ay * tSquared + 2.0f * by * t + cy;
        }
        return (ay * tCubed) + (by * tSquared) + (cy * t) + k1[1];
    }


    CNESmoothInterpolator(float InitValue = 0.0f, float Modulo = 0.0f) {
        this->Modulo = Modulo;
        SetValue(InitValue);
    }

    void Initialize(float InitValue = 0.0f, float Modulo = 0.0f) {
        this->Modulo = Modulo;
        SetValue(InitValue);
    }

    // Set an immediate value - no interpolation
    void SetValue(float Val) {
        Value = CyclicModulo(Val, Modulo);
        Time = 0;
        memset(SourceKey, 0, sizeof(SourceKey));
        memset(TargetKey, 0, sizeof(SourceKey));
        TargetKey[1] = Val;
        Valid = true;
    }

    // Get current target value
    float GetTarget() {
        return TargetKey[1];
    }

    // Get value for current time
    float GetValue() {
        if (!Valid) {
            Value = CyclicModulo(EvalCubicBezierByX(SourceKey, TargetKey, Time), Modulo);
            Valid = true;
        }
        return Value;
    }

    // Get cyclic (modulo) distance to a potential new target
    float GetDistance(float Target) {
        GetValue();
        if (Modulo != 0) {
            Target = CyclicModulo(Target, Modulo);
            if (fabs(Value - Target) > Modulo / 2) {
                Target += (Target < Value) ? +Modulo : -Modulo;
            }
        }
        return fabs(Target - Value);
    }

    // Add duration to current time
    void AdvanceTime(float Duration) {
        if (Duration > 0) {
            if (Time < TargetKey[0]) {
                Time += Duration;
                if (Time > TargetKey[0]) Time = TargetKey[0];
                Valid = false;
            }
        }
    }

    // Is target reached?
    bool TargetReached() {
        return (Time == TargetKey[0]);
    }

    // Is specific target value reached?
    bool TargetReached(float Target) {
        return (Time == TargetKey[0] && Target == TargetKey[1]);
    }

    // Set a new target to be interpolated to while duration (dampen will be clamped to 1/2 duration)
    void SetTarget(float Target, float Duration = 1.0f, float DampenIn = 0.5f, float DampenOut = 0.5f) {
        if (Duration <= 0) return SetValue(Target);

        float d[2] = {1.0 , 0.0};
        if (TargetKey[0] > SourceKey[0]) {
            Value = EvalCubicBezierByX(SourceKey, TargetKey, Time, d);
            float l = sqrt(d[0] * d[0] + d[1] * d[1]);
            if(l>0) {
                d[0] /= l;
                d[1] /= l;
            }
        }

        if (Modulo != 0) {
            Value = CyclicModulo(Value, Modulo);
            Target = CyclicModulo(Target, Modulo);
            if (fabs(Value - Target) > Modulo / 2) {
                Target += (Target < Value) ? +Modulo : -Modulo;
            }
        }

        if (DampenIn > Duration/2) DampenIn = Duration/2;
        if (DampenOut > Duration/2) DampenOut = Duration/2;
        SourceKey[0] = 0;
        SourceKey[1] = Value;
        SourceKey[2] = d[0] * DampenIn;
        SourceKey[3] = d[1] * DampenIn;
        TargetKey[0] = Duration;
        TargetKey[1] = Target;
        TargetKey[2] = -DampenOut;
        TargetKey[3] = 0;

        Valid = true;
        Time = 0;
    }

};


