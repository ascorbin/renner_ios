//
//  OMDebugView.cpp
//  comspot
//
//  Created by owi on 13.03.13.
//
//

#include "OMDebugView.h"
#include "ofApp.h"

// ------------------------------------------------
void OMDebugView::setup() {
    
    tastenfeld.loadImage("tastenfeld.png");
    trippleClickTimestamp = ofGetElapsedTimeMillis();
    trippleCount = 0;
    keyPadStartedTimestamp = ofGetElapsedTimeMillis();
    keypadMode = false;
    
    // some variables are in testApp
    ofApp* t = (ofApp*) ofGetAppPtr();
    
    
    
    // xml settings  - - - - - - - - - - - - - - - - - - - -
    
//    XML.loadFile("settings.xml");
    XML.loadFile(ofxiPhoneGetDocumentsDirectory()+"settings.xml");
    
    
    
    // appearance settings
    
    //fullscreen = XML.getValue("FULLSCREEN", false);

    
    // internal
    
    debug = XML.getValue("DEBUG", false);
    debugViewStartedTimestamp = ofGetElapsedTimeMillis();

    
	

    // listeners - - - - - - - - - - - - - - -
    
    ofAddListener(ofEvents().update, this, &OMDebugView::update);
    ofAddListener(ofEvents().draw, this, &OMDebugView::draw);
    ofAddListener(ofEvents().exit, this, &OMDebugView::exit);
    
    ofAddListener(ofEvents().keyPressed, this, &OMDebugView::keyPressed);
    
    ofAddListener(ofEvents().mousePressed, this, &OMDebugView::pressed);
    ofAddListener(ofEvents().mouseDragged, this, &OMDebugView::dragged);
    ofAddListener(ofEvents().mouseReleased, this, &OMDebugView::released);
    
    
    // UI - - - - - - - - - - - - - - - - - - - - -
    
    float hSpace = OFX_UI_GLOBAL_WIDGET_SPACING;
    float vSpace = OFX_UI_GLOBAL_WIDGET_SPACING;
    float width = 850;
    float height = 1000;
    float dim = 48;
    float blockSpace = 12;

    
    // OPTIONS - - - - - - - - - - - - - - - - - - - - -
    
    options = new ofxUICanvas(0, 0, width+hSpace, height);
    options->setColorBack(ofColor(0, 220));
    options->setVisible(debug);
    options->addWidgetDown(new ofxUILabel("OPTIONS", OFX_UI_FONT_LARGE));
    options->addSpacer(width-hSpace, 2);
    
    options->addWidgetDown(new ofxUIFPS(OFX_UI_FONT_LARGE));
    options->addWidgetDown(new ofxUIFPSSlider("FPS", width-hSpace, dim, 120));

    options->addSpacer(width-hSpace, 2);
    options->addWidgetDown(new ofxUIToggle("Bookmark deactivation only with 2 Fingers", false, dim, dim));
    options->addWidgetDown(new ofxUIToggle("Bookmark activation sound", false, dim, dim));
    options->addWidgetDown(new ofxUIToggle("Bookmark pre sense sound", false, dim, dim));
    options->addWidgetDown(new ofxUIToggle("Touch hysteresis detection", false, dim, dim));
    options->addSlider("hysteresis low value", 0, 1.0, 0.2, width-hSpace, dim);
    options->addSlider("hysteresis high value", 0, 1.0, 0.8, width-hSpace, dim);
    
    
    options->addSpacer(width-hSpace, 2);
    options->addWidgetDown(new ofxUIToggle("Back to default settings", false, dim, dim));
    
    options->addSpacer(width-hSpace, 2);
    
    ofAddListener(options->newGUIEvent, this, &OMDebugView::guiEvent);
    
    
}

//--------------------------------------------------------------
void OMDebugView::guiEvent(ofxUIEventArgs &e) {
    
    ofApp* t = (ofApp*) ofGetAppPtr();
    string name = e.widget->getName();
    int id = e.widget->getID();
    string pname = e.widget->getParent()->getName();
    
    
//    if(name == "FULLSCREEN") {
//        
//        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
//        fullscreen = toggle->getValue();
//        ofSetFullscreen(fullscreen);
//        saveSettings();
//    }
//    
//    
//    if(name == "MOUSEPOINTER") {
//        
//        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
//        mousePointer = toggle->getValue();
//        if(mousePointer) {ofShowCursor();}
//        else {ofHideCursor();}
//        saveSettings();
//    }
//    
//    
//    if(name == "BACK TO DEFAULT") {
//        
//        ofxUISlider *slider = (ofxUISlider *) e.widget;
//        backToDefaultModeTime = slider->getScaledValue();
//        backToDefaultModeTimer.setup(backToDefaultModeTime, true);
//        saveSettings();
//    }

    
}

// ------------------------------------------------
void OMDebugView::update(ofEventArgs &e) {
    
    //if(useTUIO) tuio.getMessage();
    
    if(options->isVisible()) {
        ofApp* t = (ofApp*) ofGetAppPtr();

        
        // debug mode max 5 min zeigen
        if(ofGetElapsedTimeMillis() - debugViewStartedTimestamp > 300000) {
            
            debug = false;
            options->setVisible(debug);
        }
    }

    if(ofGetElapsedTimeMillis()-keyPadStartedTimestamp > 12000) {
        
        keypadMode = false;
    };
    
}

// ------------------------------------------------
void OMDebugView::draw(ofEventArgs &e) {
    
    ofSetColor(255);
    if(keypadMode) tastenfeld.draw(0, 0);
}

// ------------------------------------------------
void OMDebugView::exit(ofEventArgs &e) {
    
    //if(useTUIO) stopTuio();
}

//--------------------------------------------------------------
void OMDebugView::saveSettings() {
    
    ofApp* t = (ofApp*) ofGetAppPtr();
    XML.setValue("DEBUG", debug);

    XML.setValue("FULLSCREEN", true);
    XML.setValue("MOUSEPOINTER", false);
 
//    XML.saveFile("settings.xml");
    XML.saveFile(ofxiPhoneGetDocumentsDirectory()+"settings.xml");
    
}

//--------------------------------------------------------------
void OMDebugView::keyPressed(ofKeyEventArgs &e){
    
}

//--------------------------------------------------------------
void OMDebugView::pressKeyD(){
    
    debugViewStartedTimestamp = ofGetElapsedTimeMillis();
    debug = !debug;
    options->setVisible(debug);
    saveSettings();
}


//--------------------------------------------------------------
void OMDebugView::pressed(ofMouseEventArgs &e){
    
    backToDefaultModeTimer.reset();
}

//--------------------------------------------------------------
void OMDebugView::dragged(ofMouseEventArgs &e){
    
    backToDefaultModeTimer.reset();
}

//--------------------------------------------------------------
void OMDebugView::released(ofMouseEventArgs &e){
    
    backToDefaultModeTimer.reset();
    
    bool clickTime = ofGetElapsedTimeMillis()-trippleClickTimestamp < 800;
    trippleClickTimestamp = ofGetElapsedTimeMillis();
    
    ofRectangle topLeft(0, 0, 200, 200);
    
    if(topLeft.inside(e.x, e.y)) {
        
        if(clickTime && trippleCount==2) {
            
            keypadMode = true;
            keyPadStartedTimestamp = ofGetElapsedTimeMillis();
            keys.clear();
            return;
        }
        else if(clickTime)trippleCount++;
        else trippleCount = 1;
    } else trippleCount = 0;
    
    // keypad
    if(keypadMode) {
        
        bool outside = false;
        bool ok_pressed = false;
        ofRectangle pad(44*2, 33*2, 388*2, 232*2);
        if(!pad.inside(e.x, e.y)) outside = true;
        float keyWidth = pad.width/4;
        float keyHeight = pad.height/3;
        int pressedX = (e.x-pad.x) / keyWidth;
        int pressedY = (e.y-pad.y) / keyHeight;
        if(pressedY==1) pressedX = (e.x-pad.x-keyWidth/2) / keyWidth;
        if(pressedY==2) pressedX--;
        int key = pressedY*4+pressedX;
        if(key==10) ok_pressed = true;
        
        if(outside) {
            keypadMode = false;
            //if(debug) ofNotifyKeyPressed('d'); // disable debug
            if(debug) pressKeyD(); // disable debug
        }
        else if(ok_pressed) {
            
            bool goodCode = true;
            if(keys.size()==CODE_LENGTH) {
                for(int i=0; i<keys.size(); i++) {
                    if(keys[i]!=keyCode[i]) goodCode = false;
                }
            }
            else goodCode = false;
            
            if(goodCode) {
                
                // right code entered
                //ofNotifyKeyPressed('d'); // enable debug mode
                pressKeyD(); // enable debug mode
            } else {
                if(debug) pressKeyD();; // disable debug
            }
            
            keypadMode = false;
        } else {
            keys.push_back(key);
        }
    }
}

