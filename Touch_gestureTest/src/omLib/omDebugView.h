//
//  OMDebugView.h
//  comspot
//
//  Created by owi on 13.03.13.
//
//

#pragma once

#include "ofMain.h"
#include "ofxXmlSettings.h"
#include "ofxTimer.h"
#include "ofxUI.h"


#define CODE_LENGTH 4
static int keyCode[CODE_LENGTH] = {1, 7, 0, 2};


class OMDebugView{
public:
    

    void setup();
    void update(ofEventArgs &e);
    void draw(ofEventArgs &e);
    void exit(ofEventArgs &e);
    
    void keyPressed(ofKeyEventArgs &e);
    void pressKeyD();
    void pressed(ofMouseEventArgs &e);
    void dragged(ofMouseEventArgs &e);
    void released(ofMouseEventArgs &e);
    
    
    ofxXmlSettings  XML;
    
    // debug panel
    
    
    
    
    
    // others & own
    
    bool debug;
    ofxTimer        backToDefaultModeTimer;
    void            saveSettings();

    unsigned long long  debugViewStartedTimestamp;
    
    // UI - - - - - - - - - - - - - - - - - - - - -
    
    ofxUICanvas  *options;
    
    void guiEvent(ofxUIEventArgs &e);

    
    // tastenfeld
    // tripple click
    ofImage tastenfeld;
    
    unsigned long long  trippleClickTimestamp;
    int             trippleCount;
    bool            keypadMode;
    vector <int>    keys;
    unsigned long long  keyPadStartedTimestamp;
    

};
