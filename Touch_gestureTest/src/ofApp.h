#pragma once


#include "ofMain.h"
#ifdef TARGET_OS_IPHONE
#include "ofxiOS.h"
#endif
#include "ofxXmlSettings.h"
#include "omScalingTool.h"
#include "omDebugView.h"
#include "scene.h"
//#include "ofxIterativeBoxBlur.h"
#include "ofxTimer.h"

#define NUM_PTS 800

#define JUSTIFICATION_LEFT      0
#define JUSTIFICATION_CENTER    1
#define JUSTIFICATION_RIGHT     2

#define WINDING_RIGHT   1
#define WINDING_LEFT   -1
#define STRAIGHT        0
#define POINT           3
#define CIRCLE          4

#ifdef TARGET_OSX
class ofApp : public ofBaseApp {
#else
    class ofApp : public ofxiOSApp {
#endif
    
	public:

		void setup();
		void update();
		void draw();
		
		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
        
        // ios
        void touchDown(ofTouchEventArgs & touch);
        void touchMoved(ofTouchEventArgs & touch);
        void touchUp(ofTouchEventArgs & touch);
        void touchDoubleTap(ofTouchEventArgs & touch);
        void touchCancelled(ofTouchEventArgs & touch);
        
        void lostFocus();
        void gotFocus();
        void gotMemoryWarning();
        void deviceOrientationChanged(int newOrientation);
        
// options
        
        bool drawTwoLines;
        bool drawLines;
        
        int backToDefaultModeTime;
        ofxTimer backToDefaultModeTimer;
        void backToDefaultEvent(ofEventArgs &e);
        
        
//        ofSoundPlayer klick;
//        ofSoundPlayer phone;
//        ofSoundPlayer foundAFigureSound;
//        
//        ofSoundPlayer navStarted;
//        //ofSoundPlayer katjaCalling;
//        ofSoundPlayer welcomeHome;
//        ofSoundPlayer phoneRinging;
//        ofSoundPlayer optionButtonKlack;
        bool playOptionButtonSounds;
        
    // blur - - - - - - - - - - - - - - - - -
    
    float blurAmount;
    const float blurMax = 70;
    const float blurSpeedPerFrameIn = 6;
    const float blurSpeedPerFrameOut = 2;
    
    
    // schon mal gut
    //ofxFboBlur gpuBlur;
    
    //ofxIterativeBoxBlur blur;
    ofFbo in_fbo;
    ofFbo out_fbo;
    
    
    // app - - - - - - - - - - - - - - - - - -
    
    bool drawMode;
    bool debug;
    //scene Scene;
        OMDebugView debugView;
    ofxXmlSettings XML;
    //ofTrueTypeFont debugFont;
    void drawScene();
    void drawDebug();
     
        bool firstFingerIsDown;
        bool secondFingerIsDown;
        
        //ofVideoPlayer mov;
        
        void checkNaviButtons(ofPoint p, bool finishedDrawing=false);
        
        bool draggingOptionsMenu;
        ofRectangle optionRect[6];
        int selectedOption;
        int clickedIntoOption;
        unsigned long long  trippleClickTimestamp;
        int             trippleCount;
    
    // stroke  - - - - - - - - - - - - - - - - -
    
    struct stroke{
        
        int type;
        float angle;
        float length;
        float timestamp;
        int id;
        ofPoint pos;
        
        float angleTolerance;
        bool found;
    };
    
    vector <stroke> strokes;
    //vector <stroke> drawStrokes;
    const float strokeTimeOut = 2.0;
    
        // one stroke
    ofVec2f dragPts[NUM_PTS];
    int pointCount;

        // two strokes
        ofVec2f dragPts1[NUM_PTS];
        int pointCount1;
        ofVec2f dragPts2[NUM_PTS];
        int pointCount2;
        
        float finger1ReleasedTimeStamp;
        float finger2ReleasedTimeStamp;
        
    void cutStroke();
    bool analyseStroke();

    vector<ofPoint> lineVertices;
    vector<float> lineAngles;
    
    
    vector <ofPolyline> lines;
    vector <int> lineType;
    
    const float sampleSpacingLength = 50;
    const float drawingLength = 20;
    const float drawingMaxRadius = 40;
    const float drawingFalloff = 0.3;
    
    // point finding constants
    ofPoint pressedLocationF1;
    ofPoint pressedLocationF2;
        
    ofPoint movedLocationF1;
    ofPoint movedLocationF2;
        
    ofPoint pressedLocation;
    float travelledDistance;
    const float maxPointDistance = 30;
    
    // line finding constants
    const float minLineLength = 70;
    const float maxLinePerAngleBias = 12;
    const float maxAngleTotalLineBias = 12;
    
    // arc finding constants
    const float minCornerAngle = 50;
    
    // arc finding constants
    const float minWindingAngle = 12;
    const float minWindingChangeAngle = 2;
    const float minArcLength = 70;
    const float minArcAngleSum = 10;
    const float maxArcAngleSum = 160;
    
    const float minCircleAngleSum = 190;
    const float maxCircleAngleSum = 540;
    
    float lengthSum;
    float angleSum;
    float angleSignedSum;
    
};

