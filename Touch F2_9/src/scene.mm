//
//  scene.cpp
//  touch
//
//  Created by Owi Mahn on 27/05/16.
//
//

#include "scene.h"
#include "ofApp.h"


//--------------------------------------------------------------
bool MyDataSortPredicate(const pair<int, float>& lhs, const pair<int, float>& rhs) {
    
    return lhs.second < rhs.second;
}

//--------------------------------------------------------------
void scene::setup(){
    
    currentComp = 0;
    currentNavigation = 0;
    
    footagePath = "2FingerTouch.psd-export/images";
    parseLayers("2FingerTouch.psd-export/2FingerTouch.psd.xml");
    parseNavigation("2FingerTouch.psd-export/2FingerTouch.navigator.xml");
    
    // load images
    
    for(int i=0; i<layerIds.size(); i++) {
        
        int d = layerIds[i];
        
        string path = imagesPath[d];
        bool isNoFont = elementFontName[d]=="";
        if(path!="" && isNoFont) {
            
            cout << "load " << imagesPath[d] << endl;
            images[d].load(imagesPath[d]);
        }
    }
    
    showComp(currentComp);
    
    
    // sort unique fontNames
    
    sort( fontNames.begin(), fontNames.end() );
    fontNames.erase( unique( fontNames.begin(), fontNames.end() ), fontNames.end() );
    
    // load fonts
    
    for(int i=0; i<fontNames.size(); i++) {
        
        vector <string> fontParts = ofSplitString(fontNames[i], ",");
        string name = fontParts[0]+".ttf";
        float size = ofToFloat(fontParts[1]) *0.75;
        bool test = usedFonts[fontNames[i]].load(name, size);
        //cout << "loadFont " << name << " : " << test << endl;
    }
    
    overlayAnim.Initialize(0);
}

//--------------------------------------------------------------
void scene::update(){
    
    updateButtonStates();
    overlayAnim.AdvanceTime(1.0/60.0);
}

//--------------------------------------------------------------
void scene::draw(){
    
    ofApp* app = (ofApp*) ofGetAppPtr();
    
    // iterate through list
    std::list<pair<int, int> >::iterator it;
    for(it = currentRenderList.begin(); it != currentRenderList.end(); it++) {
        
        int id = it->first;
        
        if(id==320) {
            
            ofSetColor(255);
            mov->draw(0, 0, 2048, 1350);
        }
        else {
            
            // draw image
            ofSetColor(255);
            if(elementFontName[id]=="") images[id].draw(imageRectangle[id].x, imageRectangle[id].y, images[id].getWidth(), images[id].getHeight());
        }
    
        // draw texts
        if(elementFontName[id]!="") {
            
            string text = elementText[id];
            
            // stunden
            if(id==3) text = ofGetTimestampString("%H");
            // minuten
            if(id==5) text = ofGetTimestampString("%M");
            
            ofSetColor(elementColor[id]);
            if(imageJustifications[id] == JUSTIFICATION_LEFT) {
                
                usedFonts[elementFontName[id]].drawString(text, imageRectangle[id].x, imageRectangle[id].y+imageRectangle[id].height);
            }
            if(imageJustifications[id] == JUSTIFICATION_CENTER) {
                
                ofRectangle textBBox = usedFonts[elementFontName[id]].getStringBoundingBox(text, 0, 0);
                float posXCorr = (float )imageRectangle[id].height * 0.15;
                float posX = imageRectangle[id].x +(imageRectangle[id].width-textBBox.width-posXCorr)/2;
                float posY = imageRectangle[id].y+imageRectangle[id].height;
                usedFonts[elementFontName[id]].drawString(text, posX, posY);
            }
            if(imageJustifications[id] == JUSTIFICATION_RIGHT) {
                
                ofRectangle textBBox = usedFonts[elementFontName[id]].getStringBoundingBox(text, 0, 0);
                float posXCorr = (float )imageRectangle[id].height * 0.15;
                float posX = imageRectangle[id].x +(imageRectangle[id].width-textBBox.width-posXCorr);
                float posY = imageRectangle[id].y+imageRectangle[id].height;
                usedFonts[elementFontName[id]].drawString(text, posX, posY);
            }
            
        }
        
        // draw debug box
        if(app->debug) {
            ofNoFill();
            if(elementFontName[id]!="") ofSetColor(0,255,255, 100);
            else ofSetColor(255, 100);
            ofDrawRectangle(imageRectangle[id]);
            ofFill();
        }
    }
    
    drawButtonStates();
    
    
    if(app->debug) {
        
//        ofSetColor(255, 220);
//        if(compNames.size()>0) {
//        app->debugFont.drawString("["+ofToString(currentComp)+"] " +compNames[currentComp], 40, 80);
//        }
        // draw navigation
        
        for(int i=0; i<currentNavIds.size(); i++) {
            
            ofNoFill();
            ofSetColor(0,80,255, 220);
            int id = currentNavIds[i];
            ofDrawRectangle(buttonRectangle[id]);
            ofDrawLine(buttonRectangle[id].getTopLeft(), buttonRectangle[id].getBottomRight());
            ofDrawLine(buttonRectangle[id].getTopRight(), buttonRectangle[id].getBottomLeft());
            ofFill();
        }
        
    }

    //drawOverlay();
    
}

//--------------------------------------------------------------
void scene::drawOverlay(){
    
    float translateY = ofMap(overlayAnim.GetValue(), 0, 1, -290, 0, true);

    ofPushMatrix();
    ofTranslate(0, translateY);
    
    // iterate through list
    std::list<pair<int, int> >::iterator it;
    for(it = overlayRenderList.begin(); it != overlayRenderList.end(); it++) {
        
        int id = it->first;
        
        // draw image
        ofSetColor(255);
        if(elementFontName[id]=="") images[id].draw(imageRectangle[id].x, imageRectangle[id].y, images[id].getWidth(), images[id].getHeight());
        

        
        // draw texts
        if(elementFontName[id]!="") {
            
            string text = elementText[id];
            
            ofSetColor(elementColor[id]);
            if(imageJustifications[id] == JUSTIFICATION_LEFT) {
                
                usedFonts[elementFontName[id]].drawString(text, imageRectangle[id].x, imageRectangle[id].y+imageRectangle[id].height);
            }
            if(imageJustifications[id] == JUSTIFICATION_CENTER) {
                
                ofRectangle textBBox = usedFonts[elementFontName[id]].getStringBoundingBox(text, 0, 0);
                float posXCorr = (float )imageRectangle[id].height * 0.15;
                float posX = imageRectangle[id].x +(imageRectangle[id].width-textBBox.width-posXCorr)/2;
                float posY = imageRectangle[id].y+imageRectangle[id].height;
                usedFonts[elementFontName[id]].drawString(text, posX, posY);
            }
            if(imageJustifications[id] == JUSTIFICATION_RIGHT) {
                
                ofRectangle textBBox = usedFonts[elementFontName[id]].getStringBoundingBox(text, 0, 0);
                float posXCorr = (float )imageRectangle[id].height * 0.15;
                float posX = imageRectangle[id].x +(imageRectangle[id].width-textBBox.width-posXCorr);
                float posY = imageRectangle[id].y+imageRectangle[id].height;
                usedFonts[elementFontName[id]].drawString(text, posX, posY);
            }
            
        }
        
    }
    ofPopMatrix();
}

//--------------------------------------------------------------
void scene::updateButtonStates(){
    
    // iterate through list
    std::map<int, int>::iterator it;
    for(it = buttonActiveImageID.begin(); it != buttonActiveImageID.end(); it++) {
        
        int id = it->first;
        if(!buttonActive[id] && buttonAlpha[id]>0) {
            
//            buttonAlpha[id] -= 20;
//            if(buttonAlpha[id]<0) buttonAlpha[id] = 0;
            buttonAlpha[id] = 0;
        }
        
    }
    
}

//--------------------------------------------------------------
void scene::drawButtonStates(){
    
    // iterate through list
    std::map<int, int>::iterator it;
    for(it = buttonActiveImageID.begin(); it != buttonActiveImageID.end(); it++) {
        
        int id = it->first;
        if(buttonAlpha[id]>0) {
            
            ofSetColor(255, buttonAlpha[id]);
            int k = buttonActiveImageID[id];
            images[k].draw(imageRectangle[k].x, imageRectangle[k].y, images[k].getWidth(), images[k].getHeight());
        }
        
    }
}

//--------------------------------------------------------------
void scene::pressButton(int buttonID){
    
    if(buttonActiveImageID[buttonID]>=0) {
        
        buttonActive[buttonID] = true;
        buttonAlpha[buttonID] = 255;
    }
}

//--------------------------------------------------------------
void scene::releaseButton(int buttonID){
    
    buttonActive[buttonID] = false;
    buttonAlpha[buttonID] = 0;
}

//--------------------------------------------------------------
void scene::releaseAllButtons(){
    
    // iterate through list

    for(auto& it : buttonActiveImageID
        ) {
        
        int id = it.first;
        buttonActive[id] = false;
    }
}

//--------------------------------------------------------------
void scene::parseLayers(string file){
    
    depth = 0;
    
    bool loaded = layersXML.load(file);
    //ofLog(OF_LOG_NOTICE, loaded?"file loaded":"not loaded");
    
    bool foundExport = layersXML.pushTag("export");
    //ofLog(OF_LOG_NOTICE, loaded?"push ok":"error: 'export' not found");
    
    
    // layers
    
    bool foundLayers= layersXML.pushTag("layers");
    //ofLog(OF_LOG_NOTICE, foundLayers?"push ok":"error: 'layers' not found");
    
    getSubLayers(footagePath);
    
    
    if(foundLayers) layersXML.popTag();
    
    
    // comps - - - - -
    
    bool foundComps= layersXML.pushTag("layerComps");
    //ofLog(OF_LOG_NOTICE, foundComps?"push ok":"error: 'layerComps' not found");
    
    int numComps = layersXML.getNumTags("layerComp");
    
    for(int i=0; i<numComps; i++) {
        
        string compName = layersXML.getAttribute("layerComp", "name", "", i);
        compNames.push_back(compName);
        
        string compLayers = layersXML.getAttribute("layerComp", "layers", "", i);
        vector <string> temp = ofSplitString(compLayers, ",");
        
        compLayersIds.push_back(temp);
    }
    
    if(foundComps) layersXML.popTag();
    
    layersXML.popTag();
    
}

//--------------------------------------------------------------
void scene::getSubLayers(string basePath){
    
    int numLayers = layersXML.getNumTags("layer");
    if(numLayers == 0) return;
    for(int i=0; i<numLayers; i++) {
        
        string layerName = layersXML.getAttribute("layer", "name", "", i);
        int layerID = layersXML.getAttribute("layer", "id", 0, i);
        
        bool hasLeft = layersXML.attributeExists("layer", "left", i);
        bool hasTop = layersXML.attributeExists("layer", "top", i);
        bool hasJustification = layersXML.attributeExists("layer", "justification", i);
        bool hasRefID = layersXML.attributeExists("layer", "refId", i);
        int posLeft = layersXML.getAttribute("layer", "left", 0, i);
        int posTop = layersXML.getAttribute("layer", "top", 0, i);
        int width = layersXML.getAttribute("layer", "width", 0, i);
        int height = layersXML.getAttribute("layer", "height", 0, i);
        
        layerIds.push_back(layerID);
        
        // put depth-values into the list
        depthList[layerID] = depth;
        depth++;
        
        elementText[layerID] = "";
        elementFontName[layerID] = "";
        
        if(hasLeft && hasTop && !hasRefID) {
            
            string layerPathName = basePath + "/" + layerName + ".png";
            imagesPath[layerID] = layerPathName;
            imageRectangle[layerID].set(posLeft, posTop, width, height);
        }
        
        if(hasJustification) {
            
            string justificationString = layersXML.getAttribute("layer", "justification", "left", i);
            string textString = layersXML.getValue("layer:text", "", i);
            
            string fontFamily = "fonts/";
            string fontFamilyStrings = layersXML.getAttribute("layer", "font-family", "", i);
            vector <string> fontFamilyString = ofSplitString(fontFamilyStrings, ",");
            if(fontFamilyString.size()>1) fontFamily += fontFamilyString[1];
            int fontSize = layersXML.getAttribute("layer", "font-size-pt", 0, i);
            
            elementText[layerID] = textString;
            elementFontName[layerID] = fontFamily+","+ofToString(fontSize);
            fontNames.push_back(fontFamily+","+ofToString(fontSize));
            
            string fontColorRawString = layersXML.getAttribute("layer", "color", "", i);
            ofStringReplace(fontColorRawString, "rgb(", "");
            ofStringReplace(fontColorRawString, ")", "");
            
            vector <string> fontColorStrings = ofSplitString(fontColorRawString, ",");
            elementColor[layerID].set(ofToInt(fontColorStrings[0]), ofToInt(fontColorStrings[1]), ofToInt(fontColorStrings[2]));
            
            int justification = JUSTIFICATION_LEFT;
            if(justificationString=="center") justification = JUSTIFICATION_CENTER;
            if(justificationString=="right")  justification = JUSTIFICATION_RIGHT;
            imageJustifications[layerID] = justification;
        }
        
        bool foundSubLayer = layersXML.pushTag("layer", i);
        
        if(foundSubLayer) {
            
            string subPath = basePath +"/"+layerName;
            if(layerName=="root") subPath = basePath;
            getSubLayers(subPath);
            layersXML.popTag();
        }
    }
}

//--------------------------------------------------------------
void scene::showComp(int compNo) {
    
    currentComp = compNo;
    
    currentIds.clear();
    currentRenderList.clear();
    
    for(int i=0; i<compLayersIds.size(); i++) {
        
        if(i==currentComp) {
            
            for(int j=0; j<compLayersIds[i].size(); j++) {
                
                currentIds.push_back(ofToInt(compLayersIds[i][j]));
                string path = imagesPath[currentIds[j]];
                if(path!="") {
                    
                    // to current render list
                    currentRenderList.push_back( make_pair(currentIds[j], depthList[currentIds[j]]) );
                }
            }
        }
    }
    
    // sort the depth list
    currentRenderList.sort(MyDataSortPredicate);
    
    
    // search if we have a navigation here
    currentNavIds.clear();
    
    for(int i=0; i<naviCompNames.size(); i++) {
        
        bool nameFound = compNames[currentComp] == naviCompNames[i];
        bool mainFound = "main" == naviCompNames[i];
        if(nameFound || mainFound) {
            
            currentNavigation = i;
            for(int j=0; j<compButtonIds[currentNavigation].size(); j++) {
                
                currentNavIds.push_back(compButtonIds[currentNavigation][j]);
            }
        }
    }
}

//--------------------------------------------------------------
void scene::showComp(string compName) {
    
    bool foundName = false;
    int foundNameID = 0;
    for(int i=0; i<compNames.size(); i++) {
        
        if(compNames[i] == compName) {
            
            foundName = true;
            foundNameID = i;
        }
    }
    if(foundName) showComp(foundNameID);
}

//--------------------------------------------------------------
void scene::showOverlay(int compNo) {
    
    
    currentOverlayIds.clear();
    overlayRenderList.clear();
    
    for(int i=0; i<compLayersIds.size(); i++) {
        
        if(i==compNo) {
            
            for(int j=0; j<compLayersIds[i].size(); j++) {
                
                currentOverlayIds.push_back(ofToInt(compLayersIds[i][j]));
                string path = imagesPath[currentOverlayIds[j]];
                if(path!="") {
                    
                    // to current render list
                    overlayRenderList.push_back( make_pair(currentOverlayIds[j], depthList[currentOverlayIds[j]]) );
                }
            }
        }
    }
    
    // sort the depth list
    overlayRenderList.sort(MyDataSortPredicate);
    
    
//    // search if we have a navigation here
//    currentNavIds.clear();
//    
//    for(int i=0; i<naviCompNames.size(); i++) {
//        
//        bool nameFound = compNames[compNo] == naviCompNames[i];
//        bool mainFound = "main" == naviCompNames[i];
//        if(nameFound || mainFound) {
//            
//            currentNavigation = i;
//            for(int j=0; j<compButtonIds[currentNavigation].size(); j++) {
//                
//                currentNavIds.push_back(compButtonIds[currentNavigation][j]);
//            }
//        }
//    }
}

//--------------------------------------------------------------
void scene::showOverlay(string compName) {
    
    bool foundName = false;
    int foundNameID = 0;
    for(int i=0; i<compNames.size(); i++) {
        
        if(compNames[i] == compName) {
            
            foundName = true;
            foundNameID = i;
        }
    }
    if(foundName) showOverlay(foundNameID);
}

//--------------------------------------------------------------
void scene::hideOverlay() {
    
    currentOverlayIds.clear();
    overlayRenderList.clear();
}

//--------------------------------------------------------------
void scene::animateOverlay(float target, float speed) {
    
    overlayAnim.SetTarget(target, speed, speed/2.0, speed/2.0);
}
    
//--------------------------------------------------------------
void scene::parseNavigation(string file){
    
    bool loaded = naviXML.load(file);
    //ofLog(OF_LOG_NOTICE, loaded?"file loaded":"not loaded");
    
    
    // drawingrect
    int b_x = naviXML.getAttribute("touchrect_for_drawing", "left", 0);
    int b_y = naviXML.getAttribute("touchrect_for_drawing", "top", 0);
    int b_w = naviXML.getAttribute("touchrect_for_drawing", "width", 2048);
    int b_h = naviXML.getAttribute("touchrect_for_drawing", "height", 1536);
    drawingRect.set(b_x, b_y, b_w, b_h);
    
    // comps - - - - -
    
    bool foundComps= naviXML.pushTag("layerComps");
    //ofLog(OF_LOG_NOTICE, foundComps?"push ok":"error: 'layerComps' not found");
    
    int numComps = naviXML.getNumTags("layerComp");
    
    for(int i=0; i<numComps; i++) {
        
        string compName = naviXML.getAttribute("layerComp", "name", "", i);
        naviCompNames.push_back(compName);
        
        naviXML.pushTag("layerComp", i);
        
        vector <int> thisCompButtons;
        int numButtons = naviXML.getNumTags("button");
        for(int j=0; j<numButtons; j++) {
            
            int buttonID = naviXML.getAttribute("button", "id", 0, j);
            string buttonName = naviXML.getAttribute("button", "name", "", j);
            string buttonTargetString = naviXML.getAttribute("button", "comp", "", j);
            int buttonActiveImageCompID = naviXML.getAttribute("button", "pressed_layer_id", -1, j);
            
            thisCompButtons.push_back(buttonID);
            
            int b_x = naviXML.getAttribute("button", "left", 0, j);
            int b_y = naviXML.getAttribute("button", "top", 0, j);
            int b_w = naviXML.getAttribute("button", "width", 0, j);
            int b_h = naviXML.getAttribute("button", "height", 0, j);
            buttonRectangle[buttonID].set(b_x, b_y, b_w, b_h);
            buttonTargets[buttonID] = buttonTargetString;
            buttonActiveImageID[buttonID] = buttonActiveImageCompID;
            buttonAlpha[buttonID] = 0;
            buttonActive[buttonID] = false;
        }
        
        naviXML.popTag();
        
        compButtonIds.push_back(thisCompButtons);
    }
    
    if(foundComps) naviXML.popTag();
    
    
}
