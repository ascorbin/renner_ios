//
//  OMDebugView.cpp
//  comspot
//
//  Created by owi on 13.03.13.
//
//

#include "OMDebugView.h"
#include "ofApp.h"

// ------------------------------------------------
void OMDebugView::setup() {
    
    tastenfeld.loadImage("tastenfeld.png");
    trippleClickTimestamp = ofGetElapsedTimeMillis();
    trippleCount = 0;
    keyPadStartedTimestamp = ofGetElapsedTimeMillis();
    keypadMode = false;
    
    // some variables are in testApp
    ofApp* t = (ofApp*) ofGetAppPtr();
    
    
    
    // xml settings  - - - - - - - - - - - - - - - - - - - -
    
//    XML.loadFile("settings.xml");
    XML.loadFile(ofxiPhoneGetDocumentsDirectory()+"settings.xml");
    

    has_bookmark = XML.getValue("HAS_BOOKMARK", false);
    bookmark_deactivate_twofingers = XML.getValue("BOOKMARK_DEACTIVATE_TWOFINGERS", false);
    bookmark_activation_sound = XML.getValue("BOOKMARK_ACTIVATION_SOUND", false);
    bookmark_presense_sound = XML.getValue("BOOKMARK_PRESENSE_SOUND", false);
    hysteresis_detection = XML.getValue("HYSTERESIS_DETECTION", false);
    hysteresis_low = XML.getValue("HYSTERESIS_LOW", 0.2);
    hysteresis_high = XML.getValue("HYSTERESIS_HIGH", 0.8);
    better_gestures = XML.getValue("BETTER_GESTURES", false);
    currentVersion = XML.getValue("VERSION", "default 4");
    can_loose_one_finger = XML.getValue("CAN_LOOSE_FINGER", false);
    checkmark_instead_of_circle = XML.getValue("USE_CHECKMARK", false);
    show_debug_strokes = XML.getValue("SHOW_DEBUG", false);
                                        
                                        
    // internal
    
    debug = false;
    debugViewStartedTimestamp = ofGetElapsedTimeMillis();

    
	

    // listeners - - - - - - - - - - - - - - -
    
    ofAddListener(ofEvents().update, this, &OMDebugView::update);
    ofAddListener(ofEvents().draw, this, &OMDebugView::draw);
    ofAddListener(ofEvents().exit, this, &OMDebugView::exit);
    
    ofAddListener(ofEvents().keyPressed, this, &OMDebugView::keyPressed);
    
    ofAddListener(ofEvents().mousePressed, this, &OMDebugView::pressed);
    ofAddListener(ofEvents().mouseDragged, this, &OMDebugView::dragged);
    ofAddListener(ofEvents().mouseReleased, this, &OMDebugView::released);
    
    
    // UI - - - - - - - - - - - - - - - - - - - - -
    
    float hSpace = OFX_UI_GLOBAL_WIDGET_SPACING;
    float vSpace = OFX_UI_GLOBAL_WIDGET_SPACING;
    float width = 850;
    float height = 1600;
    float dim = 48;
    float blockSpace = 12;

    
    // OPTIONS - - - - - - - - - - - - - - - - - - - - -
    
    options = new ofxUICanvas(0, 0, width+hSpace, height);
    options->setColorBack(ofColor(0, 220));
    options->setVisible(debug);
    options->addWidgetDown(new ofxUILabel("OPTIONS", OFX_UI_FONT_LARGE));
   
    //options->addSpacer(width-hSpace, 2);
    //options->addWidgetDown(new ofxUIFPS(OFX_UI_FONT_LARGE));
    //options->addWidgetDown(new ofxUIFPSSlider("FPS", width-hSpace, dim, 120));

    options->addSpacer(width-hSpace, 2);
    
    vector<string> versionNames = {"default 1", "default 2", "default 3", "default 4", "custom"};
    
    versions = new ofxUIRadio("VERSIONS", versionNames, OFX_UI_ORIENTATION_VERTICAL, 48, 48);
    for(int i=0; i<versionNames.size(); i++) {
        
        if(currentVersion == versionNames[i]) versions->activateToggle(versionNames[i]);
    }
    options->addWidgetDown(versions);
    
    options->addSpacer(width-hSpace, 2);
    options->addWidgetDown(new ofxUIToggle("show debug", show_debug_strokes, dim, dim));
    options->addWidgetDown(new ofxUIToggle("use Bookmark layer", has_bookmark, dim, dim));
    options->addWidgetDown(new ofxUIToggle("Bookmark deactivation only with 2 Fingers", bookmark_deactivate_twofingers, dim, dim));
    options->addWidgetDown(new ofxUIToggle("Bookmark activation sound", bookmark_activation_sound, dim, dim));
    options->addWidgetDown(new ofxUIToggle("Bookmark pre sense sound", bookmark_presense_sound, dim, dim));
    options->addWidgetDown(new ofxUIToggle("Tighter gesture separation", better_gestures, dim, dim));
    
    options->addWidgetDown(new ofxUIToggle("Can loose one finger", can_loose_one_finger, dim, dim));
    options->addWidgetDown(new ofxUIToggle("Checkmark as home gesture", checkmark_instead_of_circle, dim, dim));
    
    options->addSpacer(width-hSpace, 2);
    options->addWidgetDown(new ofxUIToggle("Touch hysteresis detection", hysteresis_detection, dim, dim));
    //options->addSlider("hysteresis low value", 0, 2.0, hysteresis_low, width-hSpace, dim);
    //options->addSlider("hysteresis high value", 0, 2.0, hysteresis_high, width-hSpace, dim);
    
    options->addSpacer(width-hSpace, 2);
    options->addWidgetDown(new ofxUIToggle("close options window", false, dim, dim));
    
    options->addSpacer(width-hSpace, 2);
    
    ofAddListener(options->newGUIEvent, this, &OMDebugView::guiEvent);
    
    setParamsForVersion(currentVersion);
}

//--------------------------------------------------------------
void OMDebugView::guiEvent(ofxUIEventArgs &e) {
    
    ofApp* t = (ofApp*) ofGetAppPtr();
    string name = e.widget->getName();
    int id = e.widget->getID();
    string pname = e.widget->getParent()->getName();
    
    
    if(name == "use Bookmark layer") {
        
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
        has_bookmark = toggle->getValue();
        saveSettings();
        versions->activateToggle("custom");
    }
    if(name == "Bookmark deactivation only with 2 Fingers") {
        
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
        bookmark_deactivate_twofingers = toggle->getValue();
        saveSettings();
        versions->activateToggle("custom");
    }
    if(name == "Bookmark activation sound") {
        
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
        bookmark_activation_sound = toggle->getValue();
        saveSettings();
        versions->activateToggle("custom");
    }
    if(name == "Bookmark pre sense sound") {
        
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
        bookmark_presense_sound = toggle->getValue();
        saveSettings();
        versions->activateToggle("custom");
    }
    if(name == "Touch hysteresis detection") {
        
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
        hysteresis_detection = toggle->getValue();
        saveSettings();
        versions->activateToggle("custom");
    }
    
//    if(name == "hysteresis low value") {
//        
//        ofxUISlider *slider = (ofxUISlider *) e.widget;
//        hysteresis_low = slider->getScaledValue();
//        saveSettings();
//    }
//    if(name == "hysteresis high value") {
//        
//        ofxUISlider *slider = (ofxUISlider *) e.widget;
//        hysteresis_high = slider->getScaledValue();
//        saveSettings();
//    }
    
    
    if(name == "Tighter gesture separation") {
        
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
        better_gestures = toggle->getValue();
        saveSettings();
        versions->activateToggle("custom");
    }
    
    if(name == "Can loose one finger") {
        
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
        can_loose_one_finger = toggle->getValue();
        saveSettings();
        versions->activateToggle("custom");
    }
    if(name == "Checkmark as home gesture") {
        
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
        checkmark_instead_of_circle = toggle->getValue();
        saveSettings();
        versions->activateToggle("custom");
    }
    
    if(name == "close options window") {
        
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
        toggle->setValue(false);
        pressKeyD();
    }
    if(name == "show debug") {
        
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
        show_debug_strokes = toggle->getValue();
        saveSettings();
        versions->activateToggle("custom");
    }
    
    if(pname == "VERSIONS") {
        
        currentVersion = name;
        setParamsForVersion(currentVersion);
        saveSettings();
    }
    //
//
//    if(name == "BACK TO DEFAULT") {
//        
//        ofxUISlider *slider = (ofxUISlider *) e.widget;
//        backToDefaultModeTime = slider->getScaledValue();
//        backToDefaultModeTimer.setup(backToDefaultModeTime, true);
//        saveSettings();
//    }

    
}

//--------------------------------------------------------------
void OMDebugView::setParamsForVersion(string v) {
    
    if(v=="default 1") {
        
        has_bookmark = false;
        bookmark_deactivate_twofingers = false;
        bookmark_activation_sound = false;
        bookmark_presense_sound = false;
        hysteresis_detection = false;
        better_gestures = false;
        can_loose_one_finger = false;
        checkmark_instead_of_circle = false;
        show_debug_strokes = false;
    }
    if(v=="default 2") {
        
        has_bookmark = true;
        bookmark_deactivate_twofingers = false;
        bookmark_activation_sound = false;
        bookmark_presense_sound = true;
        hysteresis_detection = false;
        better_gestures = false;
        can_loose_one_finger = false;
        checkmark_instead_of_circle = false;
        show_debug_strokes = false;
    }
    if(v=="default 3") {
        
        has_bookmark = true;
        bookmark_deactivate_twofingers = true;
        bookmark_activation_sound = true;
        bookmark_presense_sound = true;
        hysteresis_detection = true;
        better_gestures = true;
        can_loose_one_finger = false;
        checkmark_instead_of_circle = false;
        show_debug_strokes = false;
    }
    if(v=="default 4") {
        
        has_bookmark = true;
        bookmark_deactivate_twofingers = true;
        bookmark_activation_sound = true;
        bookmark_presense_sound = true;
        hysteresis_detection = false;
        better_gestures = true;
        can_loose_one_finger = true;
        checkmark_instead_of_circle = true;
        show_debug_strokes = false;
    }
    
    ofxUIToggle *toggle = (ofxUIToggle *) options->getWidget("use Bookmark layer");
    toggle->setValue(has_bookmark);
    toggle = (ofxUIToggle *) options->getWidget("Bookmark deactivation only with 2 Fingers");
    toggle->setValue(bookmark_deactivate_twofingers);
    toggle = (ofxUIToggle *) options->getWidget("Bookmark activation sound");
    toggle->setValue(bookmark_activation_sound);
    toggle = (ofxUIToggle *) options->getWidget("Bookmark pre sense sound");
    toggle->setValue(bookmark_presense_sound);
    toggle = (ofxUIToggle *) options->getWidget("Touch hysteresis detection");
    toggle->setValue(hysteresis_detection);
    toggle = (ofxUIToggle *) options->getWidget("Can loose one finger");
    toggle->setValue(can_loose_one_finger);
    toggle = (ofxUIToggle *) options->getWidget("Checkmark as home gesture");
    toggle->setValue(checkmark_instead_of_circle);
    toggle = (ofxUIToggle *) options->getWidget("show debug");
    toggle->setValue(show_debug_strokes);
    toggle = (ofxUIToggle *) options->getWidget("Tighter gesture separation");
    toggle->setValue(better_gestures);
    
}


// ------------------------------------------------
void OMDebugView::update(ofEventArgs &e) {
    
    //if(useTUIO) tuio.getMessage();
    
    if(options->isVisible()) {
        ofApp* t = (ofApp*) ofGetAppPtr();

        
        // debug mode max 30 sec zeigen
        if(ofGetElapsedTimeMillis() - debugViewStartedTimestamp > 30000) {
            
            debug = false;
            options->setVisible(debug);
        }
    }

    if(ofGetElapsedTimeMillis()-keyPadStartedTimestamp > 8000) {
        
        keypadMode = false;
    };
    
}

// ------------------------------------------------
void OMDebugView::draw(ofEventArgs &e) {
    
    ofSetColor(255);
    if(keypadMode) tastenfeld.draw(0, 0);
}

// ------------------------------------------------
void OMDebugView::exit(ofEventArgs &e) {
    
    //if(useTUIO) stopTuio();
}

//--------------------------------------------------------------
void OMDebugView::saveSettings() {
    
    ofApp* t = (ofApp*) ofGetAppPtr();
    XML.setValue("DEBUG", false);

    XML.setValue("HAS_BOOKMARK", has_bookmark);
    XML.setValue("BOOKMARK_DEACTIVATE_TWOFINGERS", bookmark_deactivate_twofingers);
    XML.setValue("BOOKMARK_ACTIVATION_SOUND", bookmark_activation_sound);
    XML.setValue("BOOKMARK_PRESENSE_SOUND", bookmark_presense_sound);
    XML.setValue("HYSTERESIS_DETECTION", hysteresis_detection);
    XML.setValue("HYSTERESIS_LOW", hysteresis_low);
    XML.setValue("HYSTERESIS_HIGH", hysteresis_high);
    XML.setValue("BETTER_GESTURES", better_gestures);
    XML.setValue("VERSION", currentVersion);
    XML.setValue("CAN_LOOSE_FINGER", can_loose_one_finger);
    XML.setValue("USE_CHECKMARK", checkmark_instead_of_circle);
    XML.setValue("SHOW_DEBUG", show_debug_strokes);
 
//    XML.saveFile("settings.xml");
    XML.saveFile(ofxiPhoneGetDocumentsDirectory()+"settings.xml");
    
}

//--------------------------------------------------------------
void OMDebugView::keyPressed(ofKeyEventArgs &e){
    
}

//--------------------------------------------------------------
void OMDebugView::pressKeyD(){
    
    debugViewStartedTimestamp = ofGetElapsedTimeMillis();
    debug = !debug;
    options->setVisible(debug);
    saveSettings();
}


//--------------------------------------------------------------
void OMDebugView::pressed(ofMouseEventArgs &e){
    
    backToDefaultModeTimer.reset();
}

//--------------------------------------------------------------
void OMDebugView::dragged(ofMouseEventArgs &e){
    
    backToDefaultModeTimer.reset();
}

//--------------------------------------------------------------
void OMDebugView::released(ofMouseEventArgs &e){
    
    backToDefaultModeTimer.reset();
    
    bool clickTime = ofGetElapsedTimeMillis()-trippleClickTimestamp < 800;
    trippleClickTimestamp = ofGetElapsedTimeMillis();
    
    ofRectangle topLeft(0, 0, 200, 200);
    
    if(topLeft.inside(e.x, e.y)) {
        
        if(clickTime && trippleCount==2) {
            
            keypadMode = true;
            keyPadStartedTimestamp = ofGetElapsedTimeMillis();
            keys.clear();
            return;
        }
        else if(clickTime)trippleCount++;
        else trippleCount = 1;
    } else trippleCount = 0;
    
    // keypad
    if(keypadMode) {
        
        bool outside = false;
        bool ok_pressed = false;
        ofRectangle pad(44*2, 33*2, 388*2, 232*2);
        if(!pad.inside(e.x, e.y)) outside = true;
        float keyWidth = pad.width/4;
        float keyHeight = pad.height/3;
        int pressedX = (e.x-pad.x) / keyWidth;
        int pressedY = (e.y-pad.y) / keyHeight;
        if(pressedY==1) pressedX = (e.x-pad.x-keyWidth/2) / keyWidth;
        if(pressedY==2) pressedX--;
        int key = pressedY*4+pressedX;
        if(key==10) ok_pressed = true;
        
        if(outside) {
            keypadMode = false;
            //if(debug) ofNotifyKeyPressed('d'); // disable debug
            if(debug) pressKeyD(); // disable debug
        }
        else if(ok_pressed) {
            
            bool goodCode = true;
            if(keys.size()==CODE_LENGTH) {
                for(int i=0; i<keys.size(); i++) {
                    if(keys[i]!=keyCode[i]) goodCode = false;
                }
            }
            else goodCode = false;
            
            if(goodCode) {
                
                // right code entered
                //ofNotifyKeyPressed('d'); // enable debug mode
                pressKeyD(); // enable debug mode
            } else {
                if(debug) pressKeyD();; // disable debug
            }
            
            keypadMode = false;
        } else {
            keys.push_back(key);
        }
    }
}

