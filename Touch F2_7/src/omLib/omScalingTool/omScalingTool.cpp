//
//  omScalingTool.cpp
//  comspot
//
//  Created by owi on 25.06.14.
//
//

#include "omScalingTool.h"

// Global static pointer used to ensure a single instance of the class.
omScalingTool* omScalingTool::toolInstance = NULL;

/** This function is called to create an instance of the class.
 Calling the constructor publicly is not allowed. The constructor
 is private and is only called by this Instance function.
 */

// -----------------------------------------------------------
omScalingTool* omScalingTool::getScalingTool() {
    
    // Only allow one instance of class to be generated.
    if (!toolInstance) {
        
        toolInstance = new omScalingTool;
        toolInstance->base_window.width = 1920;
		toolInstance->base_window.height = 1080;
		toolInstance->base_window.scaling = 1.0;
		toolInstance->base_window.translate_x = 0.0;
		toolInstance->base_window.translate_y = 0.0;
		toolInstance->base_window.orientation = MST_NoRotate;
		toolInstance->update();
        
        //ofAddListener(ofEvents().draw, this, &oButton::draw);
        ofAddListener(ofEvents().windowResized, toolInstance, &omScalingTool::windowResized);
    }
    return toolInstance;
}

// -----------------------------------------------------------
void omScalingTool::setup(int width, int height) {
    
    base_window.width = width;
    base_window.height = height;
    update();
}

// -----------------------------------------------------------
void omScalingTool::windowResized(ofResizeEventArgs &e) {
    
    update();
}

// -----------------------------------------------------------
void omScalingTool::update() {
    
    // get Screen dimensions
    screen_window.width = ofGetWindowWidth();
    screen_window.height = ofGetWindowHeight();
    
    screen_window.width = ofGetWindowHeight();
    screen_window.height = ofGetWindowWidth();
    
    
    if(base_window.orientation == MST_NoRotate) {
        // scale
        screen_window.scaling = min(screen_window.width/base_window.width, screen_window.height/base_window.height) * base_window.scaling;
        // center
        screen_window.translate_x = (screen_window.width - base_window.width*screen_window.scaling) / 2.0 + base_window.translate_x;
        screen_window.translate_y = (screen_window.height - base_window.height*screen_window.scaling) / 2.0 + base_window.translate_y;
        // rotate
        screen_window.rotation = 0;
    }
    if(base_window.orientation == MST_rotate180) {
        // scale
        screen_window.scaling = min(screen_window.width/base_window.width, screen_window.height/base_window.height) * base_window.scaling;
        // center
        screen_window.translate_x = screen_window.width - (screen_window.width - base_window.width*screen_window.scaling) / 2.0 + base_window.translate_x;
        screen_window.translate_y = screen_window.height - (screen_window.height - base_window.height*screen_window.scaling) / 2.0 + base_window.translate_y;
        // rotate
        screen_window.rotation = 180;
    }
    if(base_window.orientation == MST_rotate90degreesClockwise) {
        // scale
        screen_window.scaling = min(screen_window.height/base_window.width, screen_window.width/base_window.height) * base_window.scaling;
        // center
        screen_window.translate_x = screen_window.width - (screen_window.width - base_window.height*screen_window.scaling) / 2.0  + base_window.translate_x;
        screen_window.translate_y = (screen_window.height - base_window.width*screen_window.scaling) / 2.0 + base_window.translate_y;
        // rotate
        screen_window.rotation = 90;
    }
    if(base_window.orientation == MST_rotate90degreesCounterClockwise) {
        // scale
        screen_window.scaling = min(screen_window.height/base_window.width, screen_window.width/base_window.height) * base_window.scaling;
        // center
        screen_window.translate_x = (screen_window.width - base_window.height*screen_window.scaling) / 2.0  + base_window.translate_x;
        screen_window.translate_y = screen_window.height - (screen_window.height - base_window.width*screen_window.scaling) / 2.0 + base_window.translate_y;
        // rotate
        screen_window.rotation = -90;
    }
}

// -----------------------------------------------------------
ofPoint omScalingTool::scaleMouse(float x, float y) {
    
    
    float mouse_x = ((float) x - screen_window.translate_x) / screen_window.scaling;
    float mouse_y = ((float) y - screen_window.translate_y) / screen_window.scaling;
    
    if(base_window.orientation == MST_rotate90degreesClockwise) {
        mouse_x = ((float) y - screen_window.translate_y) / screen_window.scaling;
        mouse_y =  (screen_window.translate_x - (float) x) / screen_window.scaling;
    }
    if(base_window.orientation == MST_rotate90degreesCounterClockwise) {
        mouse_x = (screen_window.translate_y - (float) y) / screen_window.scaling;
        mouse_y =  ((float) x - screen_window.translate_x) / screen_window.scaling;
    }
    if(base_window.orientation == MST_rotate180) {
        mouse_x = (screen_window.translate_x - (float) x) / screen_window.scaling;
         mouse_y = (screen_window.translate_y - (float) y) / screen_window.scaling;
    }
    return ofPoint(mouse_x, mouse_y);
}

// -----------------------------------------------------------
void omScalingTool::push() {
    //glPushMatrix();
    //glTranslatef(screen_window.translate_x, screen_window.translate_y, 0);
    //glRotatef(screen_window.rotation, 0, 0, 1);
    //glScalef(screen_window.scaling, screen_window.scaling, 1.0);
    ofPushMatrix();
    ofTranslate(screen_window.translate_x, screen_window.translate_y);
    ofRotateZ(screen_window.rotation);
    ofScale(screen_window.scaling, screen_window.scaling);
}

// -----------------------------------------------------------
void omScalingTool::pop() {
    //glPopMatrix();
    ofPopMatrix();
}

// -----------------------------------------------------------
void omScalingTool::mask() {
    glPushMatrix();
    glTranslatef(screen_window.translate_x, screen_window.translate_y, 0);
    glRotatef(screen_window.rotation, 0, 0, 1);
    glScalef(screen_window.scaling, screen_window.scaling, screen_window.scaling);
    ofSetColor(0, 0, 0);
    ofRect(base_window.width, 0, base_window.width*2, base_window.height);
    ofRect(-base_window.width*2, 0, base_window.width*2, base_window.height);
    glPopMatrix();
}

// -----------------------------------------------------------
void omScalingTool::setOrientation(int orientation) {
    base_window.orientation = orientation;
    update();
}

// -----------------------------------------------------------
void omScalingTool::rotate() {
    base_window.orientation++;
    base_window.orientation%=4;
    update();
}

// -----------------------------------------------------------
void omScalingTool::scale(float scale) {
    base_window.scaling = scale;
    update();
}

// -----------------------------------------------------------
void omScalingTool::scaleUp() {
    base_window.scaling += 0.01;
    update();
}

// -----------------------------------------------------------
void omScalingTool::scaleDown() {
    base_window.scaling -= 0.01;
    update();
}

// -----------------------------------------------------------
void omScalingTool::translate(float x, float y) {
    base_window.translate_x = x;
    base_window.translate_y = y;
    update();
}

// -----------------------------------------------------------
void omScalingTool::translateUp() {
    base_window.translate_y -=1;
    update();
}

// -----------------------------------------------------------
void omScalingTool::translateDown() {
    base_window.translate_y +=1;
    update();
}

// -----------------------------------------------------------
void omScalingTool::translateRight() {
    base_window.translate_x +=1;
    update();
}

// -----------------------------------------------------------
void omScalingTool::translateLeft() {
    base_window.translate_x -=1;
    update();
}