//
//  omScalingTool.h
//  comspot
//
//  Created by owi on 25.06.14.
//
//

#pragma once

#include "ofMain.h"


//Screenrotator constants
#define	MST_NoRotate						0
#define	MST_rotate90degreesClockwise		1
#define	MST_rotate180						2
#define	MST_rotate90degreesCounterClockwise 3


// omScalingTool is a Singleton
class omScalingTool{
    
public:
    
    static omScalingTool* getScalingTool();
    
    void setup(int width, int height);
    
    void push();
    void pop();
    
    void mask();
    void setOrientation(int orientation);
    void rotate();
    void scale(float scale);
    void scaleUp();
    void scaleDown();
    void translate(float x, float y);
    void translateUp();
    void translateDown();
    void translateRight();
    void translateLeft();
    
    ofPoint scaleMouse(float x, float y);
    
    struct {
        float width;
        float height;
        float scaling;
        float rotation;
        float translate_x;
        float translate_y;
    } screen_window;
	
    struct {
        float width;
        float height;
        float scaling;
        float translate_x;
        float translate_y;
        int orientation;
    } base_window;
    
private:
    
    omScalingTool(){};  // Private so that it can  not be called
    omScalingTool(omScalingTool const&){};             // copy constructor is private
    omScalingTool& operator=(omScalingTool const&){};  // assignment operator is private
    
    static omScalingTool* toolInstance;
    
    void windowResized(ofResizeEventArgs &e);
    void update();
};
